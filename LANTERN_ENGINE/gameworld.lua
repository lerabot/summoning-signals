local graphics  = require "graphics"
local audio     = require "audio"
local input     = require "input"


local gameworld = {
  init    = function() end,
  create  = function() end,
  update  = function() end,
  render  = function() end,
  free    = function() end,
}

-- ONLY HAPPENS ONCE! EVER!
function gameworld.init()
  print("== Initialize Systems ==")
  graphics.init(640, 480, nil, canevas)
  graphics.loadFont("asset/default/spacemono.png", 15, 16)
  audio.init()
  input.init()
  collision.init()
  saveload.init()
  print("== Initialize System Done ==")
  return 1
end

-- Will be retrig on reloads
function gameworld.create()
end

function gameworld.update(dt)


  return 1
end

function gameworld.render()

  return 1
end

function gameworld.free()

end

return gameworld
