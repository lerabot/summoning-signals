local maf = require "lib.maf"
local input = {}

local DC_KEYMAP = {
  [4] = "A",
  [2] = "B",
  [1024] = "X",
  [512] = "Y",
}

local KEYMAP = {
  l = 'A',
  p = 'B',
  k = 'X',
  o = 'Y',
  start = "START",
  w     = "UP",
  s     = "DOWN",
  a     = "LEFT",
  d     = "RIGHT"
}

local KEYMAP_ARROW = {
  space = 'A',
  z     = 'B',
  x     = 'X',
  c     = 'Y',
  a     = 'LTRIG',
  s     = 'RTRIG',
  -- = "START",
  up    = "UP",
  down  = "DOWN",
  left  = "LEFT",
  right = "RIGHT",
  e     = "EDIT",
  l     = "LOG",
  q     = "QUICKSAVE"
}

local JOYMAP = {
  'A', 'B', 'X', 'Y',
  "LBUMP", "RBUMP", "SELECT", "START",
  u = "UP",
  d = "DOWN",
  l = "LEFT",
  r = "RIGHT",
}

local controller = {
  buttonPressed = {
    A = false, B = false, X = false, Y = false, START = false,
    UP = false, DOWN = false, LEFT = false, RIGHT = false,
  },
  lButton = {},
  DC_button = 0,
  DC_lbutton = 0,
  newButton = nil,
  deadzone  = 128 * 0.20,
  joy   = maf.vector(0,0),
  trig  = maf.vector(0,0),
}

local tt_inputmode = {}

local joy = nil
local pressed = {}
local ctrlMode = "DEFAULT"
local deadzone_calc = 1/(1 - controller.deadzone) -- optimization for deadzone calc

function input.init()
  if platform == "LOVE" then
    if love.joystick.getJoystickCount() > 0 then
      local joysticks = love.joystick.getJoysticks()
      joy = joysticks[1]
      for k,v in pairs(joysticks) do
        print(v:getName())
      end
      tt_inputmode = graphics.addTooltip("Gamepad ".. joysticks[1]:getName() .. " added.\nPress ESC to use keyboard", 60, 5, 4)
    end
    input.setKeymap(KEYMAP_ARROW)
    KEYMAP["return"] = "START"
  end

  if platform == "DC" then
    require "input_dc"
    KEYMAP = DC_KEYMAP
  end

  controller.lButton = copy(controller.buttonPressed)
  print("INPUT> Init done.")
end

function input.setKeymap(keymap)
  if keymap then KEYMAP = keymap end
end

function input.getPressed()
  for k, v in pairs(controller.buttonPressed) do
    --print(v)
  end
  return pressed
end

function input.setMode(mode)
  if mode == "MOUSE" then
    love.mouse.setVisible(false)
    ctrlMode = mode
  end
end

function input.getMouse()
  return love.mouse.getPosition()
end

function input.update()
  if platform == "LOVE" then
    controller.newButton = false -- leave this here, Dreamcast take care of it's on resetting
    controller.lButton = copy(controller.buttonPressed)
    _updateKeyboard()
    _updateJoystick()
  elseif platform == "DC" then
    -- gets the first controller
    local raw = {C_getController(1)}
    controller.DC_button = raw[1]
    controller.joy       :set(raw[2], raw[3])
    controller.trig      :set(raw[4], raw[5])
    _updateDreamcastController()
  end

  --_printButtons()

  -- deadzone
  local  l, deadzone = lume.round(controller.joy:length(), 0.1), controller.deadzone
  if l < deadzone then
    --controller.joy:set(0,0)
  else
    local m = -(l - deadzone) * deadzone_calc
    --controller.joy = controller.joy:scale(m);
  end
  --print("input.update: " .. tostring(controller.joy))
end

-- THE GOOD FUNCTION FOR SINGLE KEYPRESS
function input.getButton(key)
  if controller.newButton == false then return false end
  return controller.buttonPressed[key]
end

function _updateMouse(player)
  if ctrlMode == "MOUSE" then
    local x, y = input.getMouse()
    player:setPlayerPosition(x, y)

    function love.mousepressed(x, y, button, isTouch)
      if button == 1 then p1:newInput('A', true)
      else p1:newInput('A', false)
      end
    end
  end
end

function _updateKeyboard()
  function love.textinput(t)
  end

  function love.keypressed(key)
    if key == 'escape' then
      joy = nil
      graphics.clearTooltip(tt_inputmode)
      tt_inputmode = graphics.addTooltip("Keyboard mode active", 5, 5, 4)
    end
    editor.keypressed(key)
  end

  if joy ~= nil then return nil end


  for k, v in pairs(KEYMAP) do
    local button_name = v
    if love.keyboard.isDown(k) and controller.lButton[v] == false then
      controller.buttonPressed[v] = true
      controller.newButton = true
    end

    if love.keyboard.isDown(k) == false then
      controller.buttonPressed[v] = false
    end
  end
end

function _updateJoystick()
  if platform == "LOVE" and joy ~= nil then

    local axis = {joy:getAxes()}
    controller.joy:set(axis[1]*128, axis[2]*128)

    local hats = joy:getHat(1)
    for k, v in pairs(JOYMAP) do
      if string.match(hats, k) then
        if controller.lButton[v] == false then
          controller.buttonPressed[v] = true
          controller.newButton = true
        end
      elseif string.match(k, "%a") then
        controller.buttonPressed[v] = false
      end
    end

    for i, v in ipairs(JOYMAP) do
      if joy:isDown(i) == true then
        if controller.lButton[v] == false then
          controller.buttonPressed[v] = true
          controller.newButton = true
        end
      elseif joy:isDown(i) == false then
        controller.buttonPressed[v] = false
      end
    end
  end
end

function _printButtons()
  if controller.newButton then
    local i = 0
    local s = "Pressed : "
    for k, v in pairs(controller.buttonPressed) do
      if v == true then
        s = s .. k .. " "
        i = i + 1
      end
    end
    if i > 0 then
      print(s)
    end
  end

  --if controller.newButton then print("P") end
end

function input.getController(mode)
  if mode == "copy" then
    local cont = {
      buttonPressed   = copy(controller.buttonPressed),
      lButton         = copy(controller.lButton),
      DC_lButton      = controller.DC_lButton,
      newButton       = controller.newButton,
      joy             = maf.vector(0,0),
      trig            = maf.vector(0,0),
    }
    cont.joy          :set(controller.joy)
    cont.trig         :set(controller.trig)
    return cont
  else
    return controller, controller.joy, controller.trig
  end
end

function input.newInput()
  return controller.newButton
end

function input.setController(new)
  controller.buttonPressed   = copy(new.buttonPressed)
  controller.lButton         = copy(new.lButton)
  controller.DC_lButton      = new.DC_lButton
  controller.newButton       = new.newButton
  controller.joy            :clone(new.joy)
  controller.trig           :clone(new.trig)

  print("SETcontroller" .. tostring(new.joy))
end

return input
