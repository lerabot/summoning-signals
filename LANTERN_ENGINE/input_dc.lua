function _updateDreamcastController()

  controller.lButton = copy(controller.buttonPressed)
  local buttons, lButton = controller.DC_button, controller.DC_lButton

  if buttons ~= lButton then
    if (buttons & 4) ~= 0     then controller.buttonPressed["A"]      = true else controller.buttonPressed["A"] = false end
    if (buttons & 2) ~= 0     then controller.buttonPressed["B"]      = true else controller.buttonPressed["B"] = false end
    if (buttons & 1024) ~= 0  then controller.buttonPressed["X"]      = true else controller.buttonPressed["X"] = false end
    if (buttons & 512) ~= 0   then controller.buttonPressed["Y"]      = true else controller.buttonPressed["Y"] = false end

    if (buttons & 8) ~= 0     then controller.buttonPressed["START"]  = true else controller.buttonPressed["START"] = false end

    if (buttons & 16) ~= 0    then controller.buttonPressed["UP"]     = true else controller.buttonPressed["UP"] = false end
    if (buttons & 32) ~= 0    then controller.buttonPressed["DOWN"]   = true else controller.buttonPressed["DOWN"] = false end
    if (buttons & 64) ~= 0    then controller.buttonPressed["LEFT"]   = true else controller.buttonPressed["LEFT"] = false end
    if (buttons & 128) ~= 0   then controller.buttonPressed["RIGHT"]  = true else controller.buttonPressed["RIGHT"] = false end
    --if (buttons & 256) ~= 0   then controller.buttonPressed["START2"] = 1 else controller.buttonPressed["Y"] = false end

    controller.newButton = true
  else
    --print("same button")
    controller.newButton = false
  end

  -- set the lButton INT NUMBER to the last one.
  controller.DC_lButton = button
  return 1
end
