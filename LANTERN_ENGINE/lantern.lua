local lantern = {
  gameworld = {},
  loaded = false,
  libs = {
    lume        =  "lib.lume",
    flux        =  "lib.flux",
    xml         =  "lib.xml",
    json        =  "lib.json",
    maf         =  "lib.maf",
    tableToFile =  "lib.tableToFile",
    bit         =  "lib.bit",
    hump_signal =  "lib.hump_signal",
    hump_timer  =  "lib.hump_timer",

    --[[LANTERN]]--
    graphics    =  "graphics",
    gameObject  =  "gameobject",
    console     =  "console",
    input       =  "input",
    player      =  "player",
    script      =  "script",
    map         =  "map",
    dialog      =  "dialog2",
    quest       =  "quest",
    audio       =  "audio",
    log         =  "log",
    saveload    =  "saveload",
    collision   =  "collision",
    video       =  "video",
  }
}

function lantern.OLD_load()
  --[[
  if lantern.loaded == true then
    lantern.unload()
  end
  --]]
  --[[
  lume        = require "lib.lume"
  xml         = require "lib.xml"
  json        = require "lib.json"
  maf         = require "lib.maf"
  tableToFile = require "lib.tableToFile"
  bit         = require "lib.bit"

  --LANTERN--
  console     = require "console"
  input       = require "input"
  script      = require "script"
  map         = require "map"
  gameObject  = require "gameobject"
  dialog      = require "dialog"
  quest       = require "quest"
  graphics    = require "graphics"
  audio       = require "audio"
  --]]


  --LANTERN--
  --[[
  graphics    = dofile("pc/patrons.lua")
  console     = dofile("LANTERN_ENGINE/console.lua")
  input       = dofile("input")
  script      = dofile("script")
  map         = dofile("map")
  gameObject  = dofile("gameobject")
  dialog      = dofile("dialog")
  quest       = dofile("quest")
  graphics    = dofile("graphics")
  audio       = dofile("audio")
  --]]

  lantern.loaded = true
  print(">> LANTERN IS BRIGHT <<")
end


function lantern.load()
  if platform == "LOVE" then
    lantern.libs.editor = "editor"
  end


  local r = true;
  for k, v in pairs(lantern.libs) do
    --package.loaded[v] = require("pc/LANTERN_ENGINE/" .. v)
    local status, result = pcall(require, v)
    if status then
      --print("LANTERN > " .. k .. " lib loaded.")
      --Assign the required table to the global lib name. eg: graphics / audio
      _G[k] = result --IMPORTANT
      if graphics ~= nil then
        local debug = 1
        graphics.printDebug(">> LANTERN - " .. k .. " loaded.")
        graphics.renderFrame()
      end
    else
      print("LANTERN > " .. k .. " lib ERROR !!!! <----------- ")
      print(result)
      graphics.printDebug("LANTERN > " .. k .. " lib ERROR !!!! <----------- ", color.RED)
      graphics.renderFrame()
      r = false;
    end
  end
  graphics.printDebug(">> LANTERN ENGINE loaded.")
  graphics.renderFrame()
  return r

end

function lantern.unload()
  for k, v in pairs(package.loaded) do
    local moduleName = k
    if lantern.names[k] ~=  nil then
      print("Trying to delete " .. moduleName .. " module")
      package.loaded[moduleName] = nil
      v = nil
      if package.loaded[moduleName] ~= nil then
        print("Deleting the module " .. moduleName .." failed.")
      end
    end
  end
  lantern.loaded = false
  print(">> LANTERN IS DARK <<")
end

--[[
lume        = require "lib.lume"
xml         = require "lib.xml"
json        = require "lib.json"
maf         = require "lib.maf"
tableToFile = require "lib.tableToFile"
bit         = require "lib.bit"

--LANTERN--
console     = require "console"
input       = require "input"
script      = require "script"
map         = require "map"
gameObject  = require "gameobject"
dialog      = require "dialog"
quest       = require "quest"
graphics    = require "graphics"
audio       = require "audio"

print(">> LANTERN IS BRIGHT <<")
--]]

return lantern
