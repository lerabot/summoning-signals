local video = {}


function video.load(filename)
  local vid = {
    filename  = filename,
    source    = nil, -- for love.video
    length    = 0,
    isPlaying = false,
  }

  if platform == "LOVE" then
    vid.source = love.graphics.newVideo(filename)
    if vid.souce ~= nil then
    else
      print("VIDEO> Invalid video file")
    end
  else
    local f = string.sub(vid.filename, 1, -4)
    vid.filename = findFile(f .. "roq")
    print(vid.filename)
  end

  return vid
end

function video.play(vid)
  if platform == "LOVE" then
  vid.source:play()
    if p1:getButton("A") then
      vid.source:pause()
    end
  else
    C_startVideo(vid.filename)
  end

end

function video.render(vid)
  if platform == "LOVE" then
    if vid.source:isPlaying() then
      love.graphics.draw(vid.source)
    else
    end
  else
  end
end

function video.isDone(vid)
  if platform == "LOVE" then
    return not vid.source:isPlaying()
  end
end


return video
