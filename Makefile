project_name 				= summoning_signals
project_name_linux 	= summoning-signals
PROJECT_NAME 				= Summoning-Signals_V04

DATE 			= $(shell date +"%m-%d-%y")
VERSION 	= V04
TYPE 			= release

RELEASE_NAME = $(project_name)_$(VERSION)_$(DATE)
RELEASE_DIR 		= release/

DC_ENGINE 			= newdream/
project_folder 	= $(shell pwd)

EXCLUDE_TYPE 		= *.git* *.pio* *.cdi* *.iso* *.blend*
EXCLUDE_DIR			= "release/*" "dreamcast/*" "dreamcast_build/*" "newdream/*" "RAW/*"
message?="No git message :("

.PHONY: dreamcast clean-dreamcast

update-map:
	./tools/link-map.sh
	./tools/convertToDtex.sh

convert-sound:
	./convert-sound.sh

convert-sound-wav:
	./convert-sound-wav.sh

lua-test:
	lua -i conf_build.lua

clean :
	cd dreamcast

clean-dreamcast :
	cd $(DC_ENGINE) && $(MAKE) clean
	cd $(RELEASE_DIR) && rm -rf dreamcast
	@echo "Cleaned .o and release/dreamcast"

$(project_name) :
	#echo $(TYPE) > platform.lua
	rm -f $(RELEASE_DIR)*.love
	zip -9 -r $(RELEASE_DIR)/$(RELEASE_NAME).love . -x $(EXCLUDE_TYPE) $(EXCLUDE_DIR)
	love $(RELEASE_DIR)/$(RELEASE_NAME).love

package : $(project_name)

	sudo love-release -D -x $(EXCLUDE_DIR)
	#sudo love-release -D 				$(RELEASE_DIR) -x $(EXCLUDE_TYPE) $(EXCLUDE_DIR) -v $(release) 		#debian
	#sudo love-release -W 32 		$(RELEASE_DIR) -x $(EXCLUDE_TYPE) $(EXCLUDE_DIR) -v $(release)			#windows 32
	#sudo love-release -M 				$(RELEASE_DIR) -x $(EXCLUDE_TYPE) $(EXCLUDE_DIR) -v $(release)			#macosx

boon :
	mv conf.lua conf_debug.lua
	mv conf_release.lua conf.lua
	boon build .
	#love './release/boon/Summoning Signal.love'
	mv conf.lua conf_release.lua
	mv conf_debug.lua conf.lua

push-beta : boon
	butler push release/boon/Summoning\ Signals.love magnes/summoning-beta:beta

push-butler :
	butler push $(RELEASE_DIR)V$(release)/$(project_name)-win32.zip magnes/summoning-signals:win
	butler push $(RELEASE_DIR)V$(release)/$(project_name)-macos.zip magnes/summoning-signals:osx
	butler push $(RELEASE_DIR)V$(release)/$(project_name_linux)-$(release)_all.deb magnes/summoning-signals:linux

push-git : clean-dreamcast
	git add .
	git commit -m "$(message)"
	git push origin v2

pull-git : clean-dreamcast
	git pull origin v2

old-dreamcast :
	cd dreamcast && $(MAKE) console
	#$(MAKE) clean-dreamcast
	@echo "Dreamcast test is over & cleaned"

dreamcast :
	cd newdream && $(MAKE) console
	#$(MAKE) clean-dreamcast
	@echo "Dreamcast test is over & cleaned"

lxdream :
	cd newdream && $(MAKE) lxdream
	#$(MAKE) clean-dreamcast
	@echo "Dreamcast test is over & cleaned"

lxdream-nitro :
	cd newdream && $(MAKE) lxdream-nitro
	#$(MAKE) clean-dreamcast
	@echo "Dreamcast test is over & cleaned"

emulator :
	cd newdream && $(MAKE) reicast
	#$(MAKE) clean-dreamcast
	@echo "Dreamcast test is over & cleaned"

redream :
	cd newdream && $(MAKE) redream
	#$(MAKE) clean-dreamcast
	echo "Dreamcast test is over & cleaned"

build-dc :
	cd newdream && $(MAKE) buildcd
	# $(MAKE) clean-dreamcast
	echo "Dreamcast test is over & cleaned"

gdemu : build-dc
	cp -f $(RELEASE_DIR)$(PROJECT_NAME).cdi /media/magnes/GDEMU_BB/33/disc.cdi
