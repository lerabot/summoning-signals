local GFX = {}

function GFX.init()
  GFX.texture = {}
  -- Smoke
  GFX.texture.smoke = gameObject:createFromFile("asset/GFX/smoke.png")

  -- Flare
  GFX.texture.flare = gameObject:createFromFile("asset/GFX/flare.png")

  print("GFX> Init done.")
  return 1
end

function GFX.update()

end


function GFX.newSmoke(x, y, direction)

end

-- Those function should update and render at the same time
-- Also should be batched!!!
function GFX.smoke()
  local _alpha = 0

  if(frameCount % smokeSFXdelay == 0) then
    audio.play(map.sfx[1], 100, true)
    smokeSFXdelay = math.random(60, 100)
  end

  for i, v in ipairs(smoke_system) do
    v.pos.y = v.pos.y - (math.random(2, 4) * 0.1)
    v.pos.x = v.pos.x + v.dir


    --reset condition
    if v.pos.y < 100 and p1.currentQuest == QUEST_INTRO then
      v.pos.x = 260 + math.random(-4, 4)
      v.pos.y = 300
      v.scale.x = math.random(1,2)
      v.scale.y = math.random(1,2)
      v.dir = math.random(-5,5) * 0.03
    end

    _alpha = (v.pos.y - 100) * 0.004
    graphics.setDrawColor(0.5, 0.5, 0.5, _alpha)
    graphics.setTransparency(_alpha)
    v:drawObject()


    if v.pos.y < 0 then
      --table.remove(smoke_system, i)
    end
  end
  graphics.setDrawColor()
  graphics.setTransparency()
end

-- Create a new flare object and return it's function
function GFX.newFlare(x, y, intensity)
  local x, y = x, y - 30
  local f = 0 -- frame
  local flare = gameObject:copy(GFX.texture.flare)
  flare.color[4] = 1.0
  return function()

    -- Color
    local r, b
    r = math.random(7, 10) * 0.1
    b = math.random(1, 4) * 0.1
    graphics.setDrawColor(r, 0, b)

    --Scale
    local s = math.min(f*0.6, 4)
    flare.scale:set(s, s)
    if s >= 4 then
      flare.color[4] = flare.color[4] - 0.01
      graphics.setTransparency(flare.color[4])
    end
    if flare.color[4] < 0.0 then
      return false
    end

    -- Position
    if s == 4 then
      y = y - 3
    else
      y = y - 6
    end

    flare:drawObject(x, y)
    graphics.setDrawColor()
    graphics.setTransparency()
    f = f + 0.5
    return true
  end
end





return GFX
