local emitters  = {}

emitters.copper = {
  npcID       = "copper",
  position    = {370, 0},
  encoding    = hw.FM,
  difficulty  = 10,
  portrait    = nil,
  active      = true,
}

emitters.alchemist = {
  npcID       = "alchemist",
  position    = {200, 0},
  encoding    = hw.AM,
  difficulty  = 5,
  portrait    = nil,
  active      = false,
}


return emitters
