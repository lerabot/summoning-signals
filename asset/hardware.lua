--[[
HARDWARE : High level ship system handler.
Toggle (ON/OFF) and updates most system.
Handle the power and battery logic.
]]

local weather = require "weather"
local gameObject = require "gameobject"

local hw = {
  level             = 1,
  experience        = 0,
  batteryLevel      = 1500,  -- current battery level
  batteryCap        = 1500, -- max battery
  signalRefresh     = 1,
  signalStrenght    = 0.0,
  chargingSpeed     = 0.1,  -- influcent the animation and the recharge rate(3 = max)
  dischargeRate     = 1,
  autoFlipper       = 0,
  panels            = {},
  textQuality       = 0.5, --0-1 range, used for filterSignal
  display           = true,

  -- SIGNAL
  DIGI  = 3,
  FM    = 1,
  AM    = 2,
  frequency = maf.vector(230, 0),
  encoding = FM,
}

hw.signal = {
  available       = false,
  generator       = nil,
  message         = "", -- the actual message output
  filtered        = "",
  distance        = 0,
  desc            = nil, -- desc file
  desc_position   = 0,
  emitter         = {}, -- the emitter of the current signal
  nextTransmission = 0,
}

hw.status = {
  panels        = true, -- Solar pannel array
  rxSignal      = false, -- receive signal system
  txSignal      = false, -- send signal
  signalIcon    = false, -- notification if there's a signal
  radio         = false, -- Radio module

}

local emit_list = {}
local emitters = {}

local levelChart = {5, 15, 30, 60}
local mainSpeed = 25
local rFrame = 1
local tooltipID = 0
local currentSignal = nil

local battImg, battLine, battProgress, battCap

function hw.init()
  signal = require "signal"
  for i=1, 6 do
    --hw.panels[i] = hw.createPanel()
  end

  -- Battery stuff
  battImg = gameObject:createFromFile("asset/default/bat-line.png", -100, 100)
  battImg.angle = 0
  battLine = gameObject:new()
  battLine.size:set(14 + 8, 21 * 6)
  battLine.pos:set(30, 460 - battLine.size.y/2)

  battProgress = gameObject:new()
  battProgress.size:set(3, 0)
  battProgress.pos:set(60, 460 - battProgress.size.y/2)

  hw.battProgressLenght = 21 * 6
  battCap = math.floor(20 * hw.batteryLevel / hw.batteryCap)

  -- RADIO -----------------------------------------------------
  hw.initRadio()

  -- SIGNAL ----------------------------------------------------
  hw.initSignal()
  return 1
end

function hw.update(deltaTime)
  hw.updatePanels()
  --hw.updateSignal()
  hw.updateRadio()
end

function hw.render()
  if hw.display == false then return end

  hw.renderRadio()
  hw.renderSignalIcon()
end


-- BATTERY ---------------------------------------------------
function hw.chargeBattery(factor)
  local charge = (hw.chargingSpeed * factor) / mainSpeed
  hw.batteryLevel = hw.batteryLevel + charge
  hw.batteryLevel = lume.clamp(hw.batteryLevel, 0, hw.batteryCap)
end

function hw.drainBattery(factor)
  hw.batteryLevel = hw.batteryLevel - (factor / mainSpeed)
  if hw.batteryLevel < 1 then hw.batteryLevel = 0 end
end

function hw.getBattLevel()
  if hw.batteryLevel > 0 then
    return math.floor(hw.batteryLevel * 100) / 100
  else
    return 0
  end
end

function hw.renderBattery()
  local xPos, yPos = 190, 335
  local x = math.floor(20 * hw.batteryLevel / hw.batteryCap)
  local p = math.ceil((frameCount * hw.chargingSpeed ) % battCap)

  if p == 0 then
    battCap = x
  end

  -- LINE GRAPHICS
  --graphics.drawQuad(battLine, 0.25, 0.25, 0.25, 1)
  for i=1, 20 do
    if i < battCap then
      if hw.status.rxSignal == true then
        local c = color.YELLOW
        local s = math.sin(frameCount/7.0) * 0.5
        graphics.setDrawColor(c[1] - s, c[2] - s, c[3] - s,1)
      elseif hw.status.panels == true then
        if i < p then
          graphics.setDrawColor(color.ACTIVE)
        else
          graphics.setDrawColor(0,0,0,1)
        end
      elseif hw.status.panels == false then
        graphics.setDrawColor(color.ACTIVE)
      end
    else
      graphics.setDrawColor(0, 0, 0, 1)
    end
    battImg:drawObject(xPos + (i * 6), yPos)
  end

  graphics.setDrawColor()
end

-- PANELS -----------------------------------------------------
function hw.createPanel()
  local p = {
    status      = "offline",    -- broken, upgrading, online, offline
    efficiency  = 1.0,          -- floating point multiplyer
    orientation = 1.0,          -- ...
    obj         = nil,

  }
  return p
end

function hw.updatePanels()

  -- Is this ok???
  hw.status.panels = weather.isDay()

  for i, v in ipairs(hw.panels) do
    -- STATUS LOGIC
    if v.status ~= "broken" then
      if    weather.isDay() == true  then  v.status = "online"
      else                                 v.status = "offline"
      end
    end

    if v.status == "online" then
      hw.chargeBattery(3)
    end
  end
  local ori = weather.getSunOritentation()
  --print(ori)
end
--]]


-- SIGNAL -----------------------------------------------------
function hw.initSignal()
  emit_list = loadfile(findFile("asset/emitters.lua"))()
  hw.addEmitter("Copper")
  hw.addEmitter("Alchemist")
  --hw.addEmitter("alchemiasdst")

  hw.signalIcon = gameObject:createFromFile("asset/icon/signal2.png", 615, 25)
  hw.signalIcon.scale:set(0.5, 0.5)

  timer:every(0.1, hw.updateSignal)
  timer:every(4, hw.signalInArea)
end

function hw.addEmitter(name, desc_position)
  local data
  local name = string.lower(name)

  if emitters[name] ~= nil then
    print("HW> Emitter ".. name .. " already exists")
    return
  end

  if emit_list[name] then
    data = emit_list[name]
  else
    print("HW> Emitter ".. name .. " doesn't exists")
    return nil
  end

  local obj = {}
  obj.npcID             = data.npcID
  obj.desc_position     = desc_position or 0
  obj.pos               = maf.vector(data.position[1], data.position[2])
  obj.encoding          = data.encoding
  obj.difficulty        = data.difficulty or 10
  obj.active            = data.active
  obj.nextTransmission  = 0
  obj.delay             = 10

  local path = "asset/npc/" .. string.lower(name) .. ".txt"
  obj.desc_file     = findFile(path)
  if obj.desc_file == nil then
    print("HW> Couldn't load desc_file " .. path)
  end

  emitters[name] = obj

  print("HW> Added " .. name .. " to emitters.")
end

-- Updating the savefile!!!!!
function hw.updateEmitter(emit, position)
  if emitters[emit] then
    emitters[emit].desc_position = position
    return emitters[emit]
  end
end

function hw.updateSignal()

  -- Check if the signal system is online
  if hw.status.rxSignal == false then signalRefresh = 0 return nil end

  -- Drain the battery when this is working
  --hw.drainBattery(10)
  --if hw.getBattLevel() <= 0 then hw.status.rxSignal = false end

  -- Constantly refresh the signal, like a real radio
  hw.signal.available = hw.findSignal(hw.frequency, hw.encoding)

  -- If there is a signal and the generator hasn't been made.
  if hw.signal.available and hw.signal.generator == nil then
    dialog.setFile(hw.signal.emitter.desc_file, hw.signal.emitter.desc_position)
    hw.signal.message, hw.signal.author, hw.signal.trigger = dialog.getText(hw.signal.emitter)
    hw.signal.nextTransmission = realTime + 3
    hw.signal.generator = function()
      local message = hw.signal.message
      local obj     = hw.signal.emitter

      if hw.signal.nextTransmission < realTime then
        hw.signal.nextTransmission = realTime + 3
        table.remove(message, 1)
      end
      -- Return the single line if there is one.
      if message and message[1] ~= nil then
        return message[1]
      -- Set everything tot nil and set the time for the next transmission
      else

        obj.nextTransmission = realTime + obj.delay
        obj.delay = 10
        hw.signal.available = nil
        hw.signal.generator = nil
        if hw.signal.trigger then loadstring(hw.signal.trigger)() end
        return nil
      end
    end
  end
end

function hw.toggleSignal(status)
  hw.status.rxSignal = status or not hw.status.rxSignal
  print("HARDWARE> Toggle receive signal : " .. tostring(hw.status.rxSignal))
end

function hw.sendSignal()
    -- can't receive when you send
    hw.status.rxSignal = false
    hw.status.txSignal = true

    hw.radio.bouton_SIG:setTexture(hw.radio.bouton_ON)
end

-- Ca prendrait peutre un NEW et un WEAK signal. WEAK etant quelque chose qu'on a deja entendu?
function hw.signalInArea()
  hw.status.signalIcon = false

  -- This part prevents the icon from appearing in the radio screen
  if hw.status.radio == true then return end

  for k, v in pairs(emitters) do
    if v.nextTransmission < realTime and v.active == true then
      if dialog.getConditionCheck(v) then
        hw.status.signalIcon = true
      end
    end
  end
end

function hw.renderSignalIcon()
  if hw.status.signalIcon and hw.status.radio == false then
    local c = color.ACTIVE
    s = math.sin(realTime * 5) * 0.5 + 0.5

    graphics.setDrawColor(c[1],c[2],c[3],s)
    hw.signalIcon:drawObject()
    graphics.setDrawColor()
  end
end

function hw.findSignal(frequency, encoding)
  local f = frequency
  local closest, closestD = nil, 100

  for k, v in pairs(emitters) do
    local pos = v.pos
    local d = f:distance(pos)
    if d < closestD and d < v.difficulty and v.encoding == hw.encoding then
      closest, closestD = v, d
    end
  end

  -- check if closest exist and is ready for next transmission
  if closest ~= nil and closest.nextTransmission < realTime then
    --signal.available  = true
    hw.signal.emitter    = closest
    hw.signal.distance   = closestD
    return true
  else

    --signal.available  = false
    --hw.signal.emitter    = nil
    --hw.signal.distance   = 9999
    --hw.signal.message    = nil
    return false
  end
end

function hw.getEmitter(npcID)
  return emitters[npcID]
end

function hw.getEmitters()
  return emitters
end

-- RADIO ------------------------------------------------------
function hw.initRadio()
  hw.encoding = hw.AM

  hw.radio = {}
  hw.radio.map = map:load("radio", true)
  hw.radio.map.drawable = hw.radio.map.objects

  -- add bouton to a table
  hw.radio.line         = hw.radio.map:getObject("line")
  hw.radio.bouton_SIG   = hw.radio.map:getObject("boutonSIG")
  hw.radio.bouton_AM    = hw.radio.map:getObject("boutonAM")
  hw.radio.bouton_FM    = hw.radio.map:getObject("boutonFM")
  hw.radio.dial_FREQ    = hw.radio.map:getObject("dialFREQ")
  hw.radio.center_FREQ  = hw.radio.map:getObject("centerFREQ")

  hw.radio.bouton_ON  = hw.radio.map:getObject("boutonON")
  hw.radio.bouton_OFF = hw.radio.map:getObject("boutonOFF")


  hw.radio.obj = {}
  table.insert(hw.radio.obj, hw.radio.bouton_AM)
  table.insert(hw.radio.obj, hw.radio.bouton_FM)
  table.insert(hw.radio.obj, hw.radio.dial_FREQ)

  hw.radio.sfx = {}
  hw.radio.sfx[hw.FM] = audio.load("hardware/radio_FM.wav", "stream")
  hw.radio.sfx[hw.AM] = audio.load("hardware/radio_AM.wav", "stream")
  hw.radio.sfx[4] = p1.SFX[1]

  event:register("toggle_radio", hw.toggleRadio)
end

function hw.updateRadio()
  hw.radio.lock = hw.signal.available
  if hw.status.radio == false or hw.radio.lock == true then return end

  --rotate dial
  hw.radio.dial_FREQ.angle = hw.frequency.x
  hw.radio.center_FREQ.angle = hw.frequency.x
  hw.radio.line.pos.x = hw.frequency.x

  -- WATCHOUT THIS IS WHERE THE SIGNAL SWITCHING HAPPEND ---!!!!!!!!!
  if p1:holdButton("A") then

    --hw.sendSignal()
  else
    hw.status.rxSignal = true
    hw.status.txSignal = false
    hw.radio.bouton_SIG:setTexture(hw.radio.bouton_OFF)
  end

  if p1:getButton("B") then
    hw.toggleRadio(false)
  end

  if      p1:holdButton("LEFT")   then
    hw.frequency.x = lume.clamp(hw.frequency.x - 1, 175, 480)
  elseif  p1:holdButton("RIGHT")  then
    hw.frequency.x = lume.clamp(hw.frequency.x + 1, 175, 480)
  elseif  p1:getButton("UP")  then
    hw._switchEncoding(hw.FM)
  elseif  p1:getButton("DOWN")  then
    hw._switchEncoding(hw.AM)
  end
end

function hw.renderRadio()
  if hw.status.radio == false then return end

  local x, y = 320, 170

  graphics.drawQuad(bgQuad, 0, 0, 0, 0.8)
  hw.radio.map:render()

  graphics.setDrawColor(color.ACTIVE)

  --hw.renderBattery()
  --[[
  -- FM NUMBER
  if hw.encoding == hw.FM then graphics.setDrawColor(color.ACTIVE) end
  for i = 180, 480, 40 do
    graphics.print(i + hw.frequency.y, i - 10, y - 20)
  end
  graphics.setDrawColor()
  -- AM NUMBER
  if hw.encoding == hw.AM then graphics.setDrawColor(color.ACTIVE) end
  for i = 200, 500, 40 do
    graphics.print((i/2 + hw.frequency.y/2) * 0.1, i - 10, y + 20)
  end
  graphics.setDrawColor()
  --]]
  -- Transmission Text
  graphics.setDrawColor(color.BLACK)

  -- RECEIVE  MESSAGES
  if hw.status.rxSignal then
    local m = ""
    if hw.signal.generator then
      m = hw.signal.generator()
    else
      m = "...."
    end
    graphics.print(m, 153, 195, nil)
  end

  if hw.status.txSignal then
    graphics.print("TX : transmission error -- code[104]", 153, 195, nil)
  end
  graphics.setDrawColor()

  local t = "Use arrow to change frequency/encoding\nPress Z to exit radio"
  graphics.label(t, 320 - graphics.getTextWidth(t)*0.5, 400)
end

function hw.toggleRadio(state)
  hw.status.radio = state or not hw.status.radio
  hw.status.rxSignal = true
  hw.status.txSignal = false
  hw._switchEncoding(hw.encoding)
  hw.updateRadio()

  if hw.status.radio then
    p1:toggleInventory(false)
    p1:setState("radio")
    --audio.stop(currentMap.bgm)
  else
    hw.status.rxSignal = false
    hw.status.txSignal = false
    --audio.play(currentMap.bgm)
    audio.stop(hw.radio.sfx[hw.FM])
    audio.stop(hw.radio.sfx[hw.AM])
    p1:setState("idle")
  end
end

function hw._switchEncoding(encoding)
  if encoding ~= hw.encoding then
    if      encoding == hw.AM then
      audio.stop(hw.radio.sfx[hw.FM])
    elseif  encoding == hw.FM then
      audio.stop(hw.radio.sfx[hw.AM])
    end
    hw.encoding = encoding
    audio.play(hw.radio.sfx[4]) -- placeholder click sound
    --audio.play(hw.radio.sfx[hw.encoding], 1, true)
  end

  -- Switch texture
  if      encoding == hw.AM then
    hw.radio.bouton_AM:setTexture(hw.radio.bouton_ON)
    hw.radio.bouton_FM:setTexture(hw.radio.bouton_OFF)
  elseif  encoding == hw.FM then
    hw.radio.bouton_AM:setTexture(hw.radio.bouton_OFF)
    hw.radio.bouton_FM:setTexture(hw.radio.bouton_ON)
  end

  audio.play(hw.radio.sfx[hw.encoding], 1, true)
end

function hw._showtooltip()

end

-- HELPER FUNCTION --------------------------------------------
-- set the delay before the next block transmission
function hw.delayBefore(npcID, delay)
  emitters[npcID].delay = delay
end

return hw
