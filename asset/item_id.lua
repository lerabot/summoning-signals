return {
  tools = 1,
  radio = 2,
  flare = 3,
  agave = 4,
  token = 5,

  shed_key  = 10,
  fuse      = 11,
  rotor     = 12,
  waterpump = 13,

  permission    = 40,


  -- electrics
  battery_empty = 20,
  battery_full  = 21,

  -- organics
  synthethic_oil = 60,

  -- electronics
  proto_cpu = 100,
  datadisc  = 101,
  vmu       = 102,
}
