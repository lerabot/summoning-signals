local inv = {}
local item = {} -- for holding items
-- table to pass to get respons from events
action_result = {}

-- MENU
local mIndex = 0 -- index for menu
local listLength = 3

function inv.init()
  local size = 0.3

  local list = loadfile(findFile("asset/item_id.lua"))()
  for k, v in pairs(list) do
    if item[k] then
      item[k].uuid = v
    end
  end


  inv.icon = {}

  inv.icon.electric = gameObject:createFromFile("asset/icon/electric.png")
  inv.icon.electric.scale:set(size, size)
  inv.icon.mechanic = gameObject:createFromFile("asset/icon/mechanic.png")
  inv.icon.mechanic.scale:set(size, size)
  inv.icon.organic  = gameObject:createFromFile("asset/icon/organic.png")
  inv.icon.organic.scale:set(size, size)
  inv.icon.data     = gameObject:createFromFile("asset/icon/data.png")
  inv.icon.data.scale:set(size, size)

  -- Selected Item
  inv.active = false
  inv.selection = 1
  inv.selectedItem = nil

  -- Description
  inv.descriptionString = nil
  inv.descriptionDelay  = 0

  -- Item list
  inv.item = item
end

function inv.display(inventory, x, y)
  if inventory == nil then return nil end

  local xOff = 60
  local x = p1.obj.pos.x + 5
  local y = p1.obj.pos.y - 27
  if currentMap.width - x < 200 then
    x = p1.obj.pos.x - 200
  end


  local item = inventory[inv.selection]
  if item then
    graphics.setDrawColor(color.ACTIVE)
    graphics.label("Use >", x, y, color.ACTIVE)
    graphics.label(item.name, x + xOff, y, color.ACTIVE)
  end

  -- after
  for i = 1, 2 do
    item = inventory[inv.selection + i]
    if item then
      graphics.label(item.name, x + xOff, y  + (16 * i), color.LGREY)
    end
  end

  -- before
  local item = inventory[inv.selection - 1]
  if item then
    graphics.label(item.name, x + xOff, y  + (16 * -1), color.LGREY)
  end
end

function inv.renderDescription()
    -- description
    graphics.label(p1.inventory[inv.selection].name, 40, 380, color.ACTIVE)
    graphics.label(inv.descriptionString, 40, 400)

end

function inv.render()
  if p1.state == state.inventory then
    inv.renderDescription()
  end
end


function inv.update(inventory)
  --if inv.active == false then return end
  --if #inventory == 0 then return end

  if      p1:getButton("DOWN") then
    inv.selection = math.min(inv.selection + 1, #inventory)

  elseif  p1:getButton("UP") then
    inv.selection = math.max(inv.selection - 1, 1)

  -- Use the item
  elseif  p1:getButton("A") then
    inv.selectedItem = inventory[inv.selection]
    local status = inv.useItem(inv.selectedItem)
    if status == true then
      --p1:toggleInventory(false)
    else

    end
  -- Closes the inventory
  elseif  p1:getButton("B") then
    p1:toggleInventory()
  end


  mIndex = math.max(0, inv.selection - listLength)

  -- Update the item description.
  if inventory[inv.selection] then
    inv.descriptionString   = inventory[inv.selection].desc
  else
    inv.descriptionString   = "Inventory is empty"
  end

end

function inv.find(itemID)
  local t = type(itemID)
  local foundItem = nil

  if      t == "string" then
    for k, v in pairs(item) do
      if v.name == itemID then foundItem = v break end
    end
  elseif  t == "number" then
    for k, v in pairs(item) do
      if v.uuid == itemID then foundItem = v break end
    end
  elseif  t == "table" then
    for k, v in pairs(item) do
      if v.name == itemID.name then foundItem = v break end
    end
  end

  if foundItem then
    print("itemList> Found item " .. foundItem.name)
  else
    print("itemList> Can't find " .. tostring(itemID))
  end

  return foundItem
end

-- A general function to "use" the selected item.
function inv.useItem(item)
  if item == nil then return end
  local result  = false

  if item.action ~= nil then
    local r, message = item.action()

    -- Action exists but failed
    if r == false or r == nil then
      p1:playSFX("negative")
    end

    -- Turns of inventory if there's an action
    if r then
      audio.play(audio.sfx.success, 0.8)
      result = true
    end

    -- Action exist + item is consummable
    if r and item.consumable ~= nil then
      table.remove(p1.inventory, inv.selection)
      inv.selection = inv.selection - 1
      result = true
      p1:toggleInventory(false)
      p1:setState("idle")
    end
    -- The message to display on screen
    if message ~= nil then
      graphics.label_delay(message, p1.obj.pos.x + 8, p1.obj.pos.y - 24)
    end
  -- No action
  else
    p1:playSFX("negative")
  end
  return result
end

-- ITEM LIST -------------------------------------------------
item.electric  = "electric"
item.mechanic  = "mechanic"
item.organic   = "organic"
item.data      = "data"

item.template = {
  name    = "object name", -- object name
  type  = "electric", -- electric / mechanic / organic / data
  desc  = "A small electric battery.", -- item description
  value = 100, -- signal value to be used
}

item.tools = {
  name    = "Repair Kit", -- object name
  uuid  = 1,
  type  = item.mechanic, -- electric / mechanic / organic / data
  desc  = "Bertholet's handy repair kit.\nSome tools have seen better days.", -- item description
  value = 100, -- signal value to be used
  action = function()
    action_result = {}
    event:emit("use_tool", p1, action_result)
    if action_result[1] then
      return action_result[1]
    else
      return action_result[1]
    end
  end
}

item.flare = {
  name    = "Safety Flare", -- object name
  uuid  = 2,
  type  = item.mechanic, -- electric / mechanic / organic / data
  desc  = "Crack to create a bright red light.\nUse only in case of emergency.", -- item description
  consumable = true,
  value = 3, -- number of shots left
  action = function()
    action_result = {}
    event:emit("flare", action_result)
    --print("Flare result : " .. tostring(action_result[1]))
    return action_result[1]
  end
}

item.radio = {
  name    = "Radio Module", -- object name
  uuid  = 3,
  type  = item.electric, -- electric / mechanic / organic / data
  desc  = "A salvaged radio module from Sprinter-2.", -- item description
  value = 100, -- signal value to be used
  action = function()
    event:emit("toggle_radio", true)
    return true
  end
}

item.shed_key = {
  name    = "Silver Key", -- object name
  uuid  = 10,
  type  = item.mechanic, -- electric / mechanic / organic / data
  desc  = "A very old and shiny key.", -- item description
  value = 0, -- signal value to be used
  consumable = true,
  action = function()
    if p1:isOver("shed_door") then
      return true, "Door unlocked."
    else
      return false, "Can't use that."
    end
  end
}

item.vmu = {
  name    = "VMU", -- object name
  uuid  = 2,
  type  = item.electric, -- electric / mechanic / organic / data
  desc  = "An old memory card for the SEGA Dreamcast\nIt might contain some interesting data?", -- item description
  value = 3, -- number of shots left
  action = function()
    action_result = {}
    --event:emit("flare", action_result)
    --print("Flare result : " .. tostring(action_result[1]))
    return false
  end
}

item.battery_empty = {
  name    = "12v battery (empty)",
  uuid  = 4,
  type  = item.electric,
  desc  = "A small 12v electric battery.",
  consumable = true,
  action = function()
    local id = p1:getOverID()
    if id == "panel1" then
      p1:pickItem("12v battery (charged)")
      return true, "The battery is now charging"
    end
    return false, "That won't work."
  end
}

item.battery_full = {
  name    = "12v battery (charged)",
  type  = item.electric,
  desc  = "A small 12v electric battery.",
  consumable = true,
  value = 120,
  action = function()
    -- ROPER
    local id = p1:getOverID()
    if      id == "roper" then
      p1:setQuest(QUEST_ROPER, 2)
      return true, "Battery installed."
    elseif  id == "ship" then
      return true, "The battery is now charging"
    end
    return false, "That won't work."
  end
}

item.permission = {
  name  = "Royal Permission",
  type  = item.data,
  desc  = "A fancy parchemin with a beautifully drawn sun.\nAlso features a bright red stamp.",
}

item.synthethic_oil = {
  name  = "Synthetic Oil",
  type  = item.organic,
  desc  = "General purpose, fully synthetic motor oil.\n100% gooey.",
}

item.agave = {
  name    = "Agave",
  type  = item.organic,
  desc  = "A very ancient type of plant.",
  value = 70,
}

item.rotor = {
  name    = "Rotor",
  type  = item.mechanic,
  desc  = "The inside piece of a tractor motor",
  value = 70,
}

item.fuse = {
  name    = "FUZZ-EEL Fuse",
  uuid    = 12,
  type  = item.electric,
  desc  = "Maximum Amperage: 20 Amps @ 12V DC.\nElectric blue. Has a nice eel logo on it.",
  value = 70,
  consumable = true,
  action = function()
    if p1:isOver("boat1") then
      p1:setQuest(QUEST_FIXBOAT, Q_DONE)
      return true, "Gave the Fuse to Claude"
    end
    return false, "That won't work"
  end
}

item.proto_cpu = {
  name  = "Arcade CPU",
  uuid  = "1234",
  type  = item.data,
  desc  = "Harvested from a Millenium Racer arcade.\nCould be useful to repair the ship's electronics.",
  value = 70,
}

item.datadisc = {
  name  = "Data Disc",
  uuid  = "1234",
  type  = item.data,
  desc  = "Nothing written on the label",
  value = 70,
}

item.waterpump = {
  name    = "Water Pump", -- object name
  uuid  = 20,
  type  = item.mechanic, -- electric / mechanic / organic / data
  desc  = "An electric waterpump. Decent shape.", -- item description
  value = 100, -- signal value to be used
  action = function()
    --action_result = {}
    --event:emit("use_tool", p1, action_result)
    --return action_result[1]
    return false
  end
}

item.token = {
  name    = "KNOT token", -- object name
  type  = item.mechanic, -- electric / mechanic / organic / data
  desc  = "A silver coin with KNOT engraved on it.", -- item description
  value = 1, -- signal value to be used
  action = function()
    local t = p1:getOver()
    if t then
      if t.npcID == "arcade" then
      event:emit("toggle_dragon_arcade")
      return true, "You insert the token in the arcade"
      end
    end
    return false, "That won't work."
  end
}

return inv
