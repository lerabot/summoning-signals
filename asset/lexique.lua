local lexique = {}

lexique.bases = {
  "Sprinter-1",
  "B. & A. Strugatsky",
  "Redrick",
  "Radiona",
  "Tariel-VI",
  "", "", "ERR0R104", "..*8**....", "", "",
}

lexique.subjects = {
  "Copper",
  "Zinc",
  "Salt",
  "Gold",
  "Silver",
  "Mercury",
  "Grand-pic",
  "Barbacou",
  "Petit-duc",
  "Maitre Corbeau",
  "Minotaur"
}

lexique.objects = {
  "radio",
  "pannels",
  "wrench",
  "vials",
  "leftover bolts",
  "broken screen",
  "uncomplete map",
  "the fire",
  "other book",
  "worms",
  "crystals",
  "rocks",
  "metals",
  "sand",
  "nest",
  "whispers"
}

lexique.situations = {
  "the grand fall",
  "forever",
  "in time",
  "at dawn",
  "during sunrise"
}

lexique.verbs = {
  "whispers",
  "forgets",
  "burns",
  "floods",
  "risks",
  "melts",
  "floats",
  "lies",
  "repairs",
  "fixes",
  "breaks",
  "summons",
  "casts",
  "listens",
  "harvests",
  "mines",
  "collects",
}

lexique.links = {
  "and",
  "to",
  "with",
  "will",
  "for",
  ">  ",
}

lexique.adjectives = {
  "slippery",
  "grand",
  "great",
  "tiny",
  "majestic",
  "quiet",
  "pitious",
  "graceful",
  "incomplete",
  "forever"
}

lexique.adverbs = {
  "truly",
  "slowly",
  "terribly",
  "softly",
  "kindly",
  "quickly",
  "unwillingly",
  "strangly",
  "purely",
  "freely"
}

lexique.structures = {
  {mainSubject, links, subjects},
  {mainSubject, verbs, adverbs},
  {mainSubject, verbs, adjectives, objects},
  {subjects, verbs, objects},
  {subjects, verbs, objects, "and", objects},
  {subjects, adjectives, objects},
  {subjects, verbs},
  {adverbs, verbs, situations},
  {verbs, objects, situations},
  {objects, "and", objects},
  {situations, verbs},
  {situations, mainSubject, verbs},
  {links, adjectives, subjects},
  {links, subjects, verbs},
  {links, mainSubject, verbs},
  {adjectives, mainSubject},
  {adjectives, objects},
}

return lexique
