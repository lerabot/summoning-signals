local script = script:new()

local stencil
local bg, water, water2

function script:onLoad()
  local map = self.parent

  --bg = gameObject:createFromFile("asset/map_agora/bg_large.png", 512, 256)
  --map:addObject(bg)

  water = gameObject:createFromFile("asset/map_agora/water_mask_small.png", 512, 256)
  water.scale:set(2,2)

  --p1.obj.pos:set(map.width/2, map.height/2)

  --self:activate()
  return 1
end

function script:activate()
  water = gameObject:createFromFile("asset/map_agora/water_mask_small.png", 512, 256)
  water.scale:set(2,2)
end

function script:desactivate()
  graphics.freeTexture(water.texture)
end

function script:render()
  graphics.setClearColor(0, 0.1, 0.36, 1)

  graphics.setDrawColor(0, 0, 1, 1)
  graphics.setTransparency(0.3)
  water:drawObject()
  graphics.setDrawColor()
  graphics.setTransparency()

  --graphics.drawTexture(water.texture, water, 256, 256)
  --graphics.drawMultiTexture(logo.texture, logo, blue.texture, blue, 512, 256)
end

return script
