local script = script:new()

local map
local screen1, screen2

function script:onLoad()
  map = self.parent
  screen1 = map:getObject("screen1")
  screen2 = map:getObject("screen2")

  map:getObject("salvage1").canRepair = 1

end

function script:activate()
end

function script:desactivate()
end

function script:update()
  local day = weather.isNight()
  screen1.active = day
  screen2.active = day

end

function script:render()

  if weather.isDay() == false then
    local glow = math.random(6, 7) * 0.1
    graphics.setDrawColor(glow , glow, glow, 1)
    screen1:drawObject()
    glow = math.random(6, 7) * 0.1
    graphics.setDrawColor(glow , glow, glow, 1)
    screen2:drawObject()
    graphics.setDrawColor()
  end

end

return script
