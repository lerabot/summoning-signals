local script = script:new()

local map
local bg
local alchemist, worker, guard
local rope1, roper

function script:onLoad()
  map = self.parent
  bg = gameObject:new()

  --p1:pickItem("Royal Permission")
  map.bgm = audio.load("bgm/echelles_explo2.ogg", "stream")

  --Roper test
  roper = map:getObject("roper")
  p1:setPosition(roper.pos.x+30, roper.pos.y)

  -- Guard
  guard = map:getObject("guard")
  guard:addAnimation("templar", {1,26,51,76}, nil)

  -- Worker
  worker = map:getObject("worker")
  worker:addAnimation("worker", {265,270,275,280}, "pingpong")

  map.drawObjectFlag = false

  rope1 = map:getObject("rope")
  rope1.scale:set(0.3, 0.3)
  rope1.pos.y = rope1.pos.y - 10
  map:removeObject("rope")

  local t = map:addTrigger(map:getObject("guard"), nil, "REPEAT")
  event:register("castlepath_permission", function()
    table.remove(map.trig, t)
    print("Can pass freely")
  end)
end

function script:activate()
  if p1:hasQuest(QUEST_DRAGON) then
    map.bgm = audio.load("bgm/echafaud-beat.ogg", "stream")
  end

  hw.getEmitter("alchemist").active = true

  if godmode then
  end


end

function script:desactivate()
  hw.getEmitter("alchemist").active = false
end

function script:update()
  local r = p1.obj.pos.y / self.parent.height
  local b = p1.obj.pos.y / self.parent.height
  weather.setClearColor(1 - r, b/3, b, 1)

  worker:updateAnimation()
  guard:updateAnimation(20)


  local shift = r * 50
  bg.pos = graphics.camera.pos + graphics.camera.size/2
  bg.pos.y = bg.pos.y + 100 - shift
end

function script:render()
  graphics.setDrawColor(draw_color)
  map.drawObjectFlag = true
  map:render()
  map.drawObjectFlag = false

  graphics.setDrawColor(draw_color)
  self:_renderRope()

end

function script:_renderRope()
  local x, y = rope1.pos.x, rope1.pos.y
  local yOff = 0

  if roper.canRepair == ITEM_REPAIRED then
    yOff = (frameCount / 10) % 16
  end

  rope1:drawObject(x, y)
  for i=0, 32 do
    rope1:drawObject(x, y + (i * 16) + yOff)
  end
  roper:drawObject()

end

function script:_updateRoper()
  -- bootleg thing to make the leg position behave like the
  roper_leg.desc_position = math.max(roper_leg.desc_position, roper.desc_position)
end

return script
