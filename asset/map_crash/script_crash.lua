local script = script:new()


local ship
local smoke
local smoke_system = {active = true}
local smokeSFXdelay = 1
local flare, e_flare

local map -- reference to self map

function script:onLoad()
  map = self.parent

  ship = map:getObject("ship")
  ship:setNpc("ship")
  map:addTrigger(ship, nil, "ONCE")
  saveload:addNPC(MAP_CRASH, ship)

  smoke = gameObject:createFromFile("asset/GFX/smoke.png", 250, 300)

  map.sfx[1] = audio.load("fire_short.wav", "sfx")
  map.bgm = audio.load("bgm/crash.ogg", "stream")

  local maxSmoke = 50
  if platform == "DC" then maxSmoke = 50 end

  for i=1, maxSmoke do
    local s = gameObject:copy(smoke)
    s.pos.x = ship.pos.x + 30 + math.random(-20, 20)
    s.pos.y = ship.pos.y + math.random(-200, 0)
    s.scale.x = math.random(1,2)
    s.scale.y = math.random(1,2)
    s.angle = math.random(360)
    s.dir = math.random(-5,5) * 0.03
    table.insert(smoke_system, s)
  end

  flare = {}
  e_flare = event:register("flare", script._flareShot)
  map.canWarp = false


end

function script:activate()
  local progress = p1:hasQuest(QUEST_CRASH)
  print(progress)
  if progress == Q_DONE then
    map.canWarp = true
  end
end

function script:desactivate()
  audio.stop(map.sfx[1])
end

function script:update()
end

function script:render()
  -- Smoke
  if p1:hasQuest(QUEST_CRASH) then script:_smoke() end

  -- Flare effect
  for i, v in ipairs(flare) do
    local r = v()
    if r == false then table.remove(flare, i) end
  end
end

function script:_smoke()
  if p1.obj.pos:distance(ship.pos) > 600 then return nil end
  local _alpha = 0
  local x, y = ship.pos.x + 40, ship.pos.y - 20

  if(frameCount % smokeSFXdelay == 0) then
    audio.play(map.sfx[1], 100, false)
    smokeSFXdelay = math.random(60, 100)
    --print("trigger sound")
  end

  graphics.startBatch(smoke.texture)
  for i, v in ipairs(smoke_system) do
    local v = v
    v.pos.y = v.pos.y - (math.random(2, 4) * 0.1)
    v.pos.x = v.pos.x + v.dir


    --reset condition
    if v.pos.y < y - 250 then
      v.pos.x = x + math.random(-4, 4)
      v.pos.y = y
      v.scale.x = math.random(1,2)
      v.scale.y = math.random(1,2)
      v.dir = math.random(-5,5) * 0.03
    end

    --_alpha = (v.pos.y - (y - 250)) * 0.004
    graphics.setDrawColor(0.5, 0.5, 0.5, 1)
    --graphics.setTransparency(_alpha)
    --v:drawObject()
    graphics.addToBatch(v)


    if v.pos.y < 0 then
      --table.remove(smoke_system, i)
    end

  end
  graphics.endBatch(smoke.texture)
  graphics.setDrawColor()
  graphics.setTransparency()
end

function script:_toggleSmoke()
  smoke_system.active = not smoke_system.active
end


function script._flareShot(arg)
  print(arg)
  if p1:hasQuest(QUEST_COPPER) then
    table.insert(flare, GFX.newFlare(p1.obj.pos.x, p1.obj.pos.y, 3))
    p1.quests[QUEST_COPPER].progress = 2
    event:remove("flare", e_flare)
    table.insert(arg, true)
  else
    table.insert(arg, false)
    --event:emit("set_description", "Why waste a precious flare?")
  end

end

return script
