local script = script:new()

local dragon, d_left, d_right
local attacker
local map
local cave, cavegrid, yscroll, cZone
local dragon_life, life = 10, 5
local dmg_mod = 0.3
local MAX_CHARGE = 40

local ship
local p1bullet, p1charge = {}, 0
local bullet

local b1, b2 -- textures
local b = {
  pos   = maf.vector(0,0),
  dir   = maf.vector(0,0),
  size  = maf.vector(0,0),
}

local title = {}


function script:onLoad()
  map = self.parent
  map.drawObjectFlag = false

  ship = map:getObject("ship")

  dragon = map:getObject("dragon")
  dragon.bullet = {}
  dragon.state = "title"
  dragon.original_pos = maf.vector(320, -400)

  d_left = map:getObject("d_left")
  d_left.offset = d_left.pos - dragon.pos
  d_left.flash = 0
  d_left.life  = 100
  d_right = map:getObject("d_right")
  d_right.offset = d_right.pos - dragon.pos
  d_right.flash = 0
  d_right.life  = 100

  b1 = map:getObject("b1")
  b2 = map:getObject("b2")
  b1.scale:set(1.5, 1.5)

  title[1] = map:getObject("title")
  title[2] = map:getObject("start")
  title[3] = map:getObject("knot")
  title[4] = map:getObject("credit")
  title[5] = map:getObject("gold1")
  title[6] = map:getObject("gold2")
  title[7] = map:getObject("gold3")
  title[8] = map:getObject("dragonTitle")

  for i=1, 10 do
    --local _b = copy(b)
    --_b.pos:set(dragon.pos.x, dragon.pos.y)
  end

  yscroll = 0

  event:register("dragon_mode", function(state)
    if state == "game" then
      p1:setState("arcade")
    end
    dragon.state = state
  end)

end

function script:activate()
  cave = gameObject:createFromFile("asset/map_dragon/cave.png")
  cave.scale:set(3,3)

  bullet = gameObject:createFromFile("asset/map_dragon/bullet.png")

  p1.state = 99 --dragon
  p1:setPosition(320, 400)
  p1:setVisible(false)

  graphics.setCamTarget(nil)
  graphics.setCamPosition(320, 240)

  self:restartGame()
  dragon.state = "title"
  p1:setState("arcade")
end

function script:desactivate()
  cave:delete()
  bullet:delete()
end

function script:update()
  if     dragon.state == "title" then
    p1:setState("arcade")
    if p1:getButton("START") then
      script:switchTo("intro")

    end
  elseif dragon.state == "intro" then
    self:updateHeads()

  elseif dragon.state == "game" then
    if p1.state ~= state.arcade then p1:setState("arcade") end
    weather.setClearColor (1,0,0,1)
    weather.setDrawColor  (1,1,1,1)

    self:updateDragonAttack()
    self:updateHeads()
    self:updateBullets()
    self:_containPlayer()

  elseif dragon.state == "dialog" then
  elseif dragon.state == "win" then
    self:updateHeads()
    if p1:getButton("A") then
      self:restartGame()
    end
  elseif dragon.state == "defeat" then
    weather.setClearColor(1,0,0,1)
    if p1:getButton("A") then
      self:restartGame()
    end
  end
end

function script:render()

    if     dragon.state == "title" then
      script:titleScreen()
    elseif dragon.state == "intro" then
      self:renderCave()
      self:renderDragon()
      self:renderShip()
    elseif dragon.state == "game" then
      self:renderCave()
      self:renderLife()
      self:renderDragon()
      self:renderShip()

      graphics.setDrawColor(1,1,1,1)
      for i, v in ipairs(dragon.bullet) do
        --graphics.drawRect(v.pos.x, v.pos.y, 10, 10)
        b1:drawObject(v.pos.x, v.pos.y)
      end
      graphics.setDrawColor()

      if p1:holdButton("A") then
        bullet.scale:set(p1charge*0.1, 0.5)
        bullet:drawObject(p1.obj.pos.x, p1.obj.pos.y-20)
      end

      for i, v in ipairs(p1bullet) do
        s = v.dmg
        if s == MAX_CHARGE then
          bullet.scale:set(2.0, 3.0)
          bullet:drawObject(v.pos.x, v.pos.y)
        else
          bullet.scale:set(1.0, 2.0 + (s * 0.1))
          bullet:drawObject(v.pos.x, v.pos.y)
        end
      end

      if ship.hit > 0 then
        sleep(3)
        ship.scale:set(1.0 - ship.hit*0.1, 1.0 - ship.hit*0.1)
        graphics.setDrawColor(0.3,0.3,0.3,1)
        ship:drawObject()
        graphics.setDrawColor()
        ship.hit = math.max(0, ship.hit - 1)
        ship.immuneUntil = realTime + 1.5
      end


    elseif dragon.state == "dialog" then
      self:renderCave()
      self:renderLife()
      self:renderDragon()
      self:renderShip()
    elseif dragon.state == "win" then
      self:renderCave()
      local t = "ACID DRAGON DEFEATED"
      graphics.label(t, 320 - graphics.getTextWidth(t)*0.5, 240, color.ACTIVE)
      self:renderDragon()
      ship:drawObject()
    elseif dragon.state == "defeat" then
      self:renderCave()
      self:renderDragon()
      ship:drawObject()
      local t = "THE SOUL STILL BURNS"
      graphics.label(t, 320 - graphics.getTextWidth(t)*0.5, 240)

      p1bullet = {}
      dragon.bullet = {}
      dragon_life = 400
      life = 5
    end
end

function script:updateDragonAttack()
  if      dragon_life > 300 then
    if d_left.life <= 0 then return end
    -- Dragon Position
    dragon.pos.x = 320 + math.sin(realTime) * 25
    if dragon.attack_delay1 < realTime then
      local f = maf.vector(0, 1)
      dragon:addForce(f, 17, true)
      dragon.attack_delay1 = realTime + 5
      dragon.recover       = realTime + 2
    end

    -- Recover
    if dragon.recover < realTime then
      local f = maf.vector(0, -1)
      dragon:addForce(f, 17, true)
      dragon.recover = realTime + 9999
    end

    -- Adding Bullet
    if frameCount % 4 == 0 and #dragon.bullet < 10 then
      local dX, dY    = d_left.pos.x, d_left.pos.y
      local toPlayer  = p1.obj.pos - attacker.pos
      toPlayer:normalize():scale(2)
      local bullet = {
        pos = maf.vector(attacker.pos.x, attacker.pos.y),
        --dir = maf.vector(math.random(-10, 10) * 0.3, 3),
        dir = toPlayer
      }

      bullet.dir:scale(3)
      --bullet.dir = bullet.dir + toPlayer
      table.insert(dragon.bullet, bullet)
    end
  elseif  dragon_life < 300 and dragon_life > 150 then
    if d_right.life <= 0 then return end
    -- Dragon Position
    dragon.pos.x = 320 + math.sin(realTime * 3) * 30
    if dragon.attack_delay1 < realTime then
      local f = maf.vector(0, 1)
      dragon:addForce(f, 17, true)
      dragon.attack_delay1 = realTime + 5
      dragon.recover       = realTime + 2
    end

    -- Recover
    if dragon.recover < realTime then
      local f = maf.vector(0, -1)
      dragon:addForce(f, 17, true)
      dragon.recover = realTime + 9999
    end

    -- Adding Bullet
    if frameCount % 6 == 0 and #dragon.bullet < 20 then
      local dX, dY    = d_right.pos.x, d_right.pos.y
      local toPlayer  = p1.obj.pos - attacker.pos
      toPlayer:normalize():scale(2)
      local bullet = {
        pos = maf.vector(attacker.pos.x, attacker.pos.y),
        dir = maf.vector(math.random(-10, 10) * 0.3, 3),
        --dir = toPlayer
      }

      --bullet.dir:scale(3)
      bullet.dir = bullet.dir + toPlayer
      table.insert(dragon.bullet, bullet)
    end
  elseif  dragon_life < 150 then
    -- Dragon Position
    dragon.pos.x = 320 + math.sin(realTime * 3) * 60
    if dragon.attack_delay1 < realTime then
      local f = maf.vector(0, 1)
      dragon:addForce(f, 17, true)
      dragon.attack_delay1 = realTime + 3
      dragon.recover       = realTime + 1
    end

    -- Recover
    if dragon.recover < realTime then
      local f = maf.vector(0, -1)
      dragon:addForce(f, 17, true)
      dragon.recover = realTime + 9999
    end

    -- Adding Bullet
    -- LEFT
    if frameCount % 2 == 0 and #dragon.bullet < 10 and d_left.life > 0 then
      local dX, dY    = d_left.pos.x, d_left.pos.y


      local toPlayer  = p1.obj.pos - maf.vector(dX, dY)
      toPlayer:normalize():scale(2)
      local bullet = {
        pos = maf.vector(d_left.pos.x, d_left.pos.y),
        dir = maf.vector(math.random(-3, 3) * 0.3, 5),
        --dir = toPlayer
      }

      --bullet.dir:scale(3)
      bullet.dir = bullet.dir + toPlayer
      table.insert(dragon.bullet, bullet)
    end

    -- RIGHT
    if frameCount % 2 == 1 and #dragon.bullet < 10 and d_right.life > 0 then
      local dX, dY    = d_right.pos.x, d_right.pos.y
      local toPlayer  = p1.obj.pos - maf.vector(dX, dY)
      toPlayer:normalize():scale(2)
      local bullet = {
        pos = maf.vector(dX, dY),
        dir = maf.vector(math.random(-3, 3) * 0.3, 5),
        --dir = toPlayer
      }

      --bullet.dir:scale(3)
      bullet.dir = bullet.dir + toPlayer
      table.insert(dragon.bullet, bullet)
    end
  end

      dragon:updatePosition()

end

function script:updateBullets()
  local p1x, p1y = p1.obj.pos.x, p1.obj.pos.y
  local immunity = false

  if realTime > ship.immuneUntil then
    immunity = true
  end

  for i, v in ipairs(dragon.bullet) do
    v.pos = v.pos + v.dir
    if sh4_distance(v.pos.x, v.pos.y, p1x, p1y) < 12 and immunity then
      life = life - 1
      ship.hit = 5
      table.remove(dragon.bullet, i)
      if life == 0 then
        dragon.state = "defeat"
      end
    end

    if v.pos.y > 1000 then
      dragon.bullet = {}
      if attacker == d_left then
        attacker = d_right
      else
        attacker = d_left
      end
    end
  end

  -- PLAYER ------------------------------
  if p1:getButton("A") then
    local _b = {
      pos = maf.vector(p1x, p1y),
      dir = maf.vector(0, -5),
      dmg = 5,
    }
    table.insert(p1bullet, _b)
  end

  if p1:holdButton("A") then
    p1charge = math.min(p1charge + 1, MAX_CHARGE)
  else
    p1charge = math.max(p1charge - 3, 1)
  end


  if p1charge == MAX_CHARGE then
    local _b = {
      pos = maf.vector(p1x, p1y),
      dir = maf.vector(0, -5),
      dmg = MAX_CHARGE,
    }
    table.insert(p1bullet, _b)
    p1charge = 1
  end

  for i, v in ipairs(p1bullet) do
    v.pos = v.pos + v.dir
    if sh4_distance(v.pos.x, v.pos.y, d_left.pos.x, d_left.pos.y) < 40 then
      d_left.flash = 5
      table.remove(p1bullet, i)
      --dragon_life = dragon_life - (v.dmg * dmg_mod)
      d_left.life = d_left.life - (v.dmg * dmg_mod)
    end

    if sh4_distance(v.pos.x, v.pos.y, d_right.pos.x, d_right.pos.y) < 40 then
      d_right.flash = 5
      table.remove(p1bullet, i)
      d_right.life = d_right.life - (v.dmg * dmg_mod)
      --dragon_life = dragon_life - (v.dmg * dmg_mod)
    end

    if v.pos.y < 10 then
      table.remove(p1bullet, i)
    end
  end
end

function script:renderShip()
  graphics.setDrawColor(0, 0, 0, 0.4)
  local x, y = ship.pos.x - ((ship.scale.x-1.5) * 40), ship.pos.y + 5
  ship:drawObject(x, y)
  graphics.setDrawColor()

  ship:drawObject()
end


function script:updateHeads()
  if dragon.state == "win" then


  else
    if d_left.life <= 0 then
      d_left:addForce(maf.vector(1, 1), 0.5)
      d_left:updatePosition()
      d_left.angle = d_left.angle + 3
    else
      d_left.pos = dragon.pos + d_left.offset
      d_left.angle = 0 + math.sin(realTime) * 10
    end

    if d_right.life <= 0 then
      d_right:addForce(maf.vector(-1, 1), 0.5)
      d_right:updatePosition()
      d_right.angle = d_right.angle - 3
    else
      d_right.angle = 0 + math.sin(realTime) * -10
      d_right.pos = dragon.pos + d_right.offset
    end
  end




  --[[
  if attacker == d_left then
    d_left.scale:set(d_left.scale.x + 0.01, d_left.scale.y + 0.01)
    d_right.scale:set(1,1)
  else
    d_right.scale:set(d_right.scale.x + 0.01, d_right.scale.y + 0.01)
    d_left.scale:set(1,1)
  end
  --]]
end

function script:_renderHeads()
  if d_left.flash > 0 then
    graphics.setDrawColor(1,0,0,1)
    d_left:drawObject()
    graphics.setDrawColor()
    d_left.flash = d_left.flash - 1
  else
    d_left:drawObject()
  end

  if d_right.flash > 0 then
    graphics.setDrawColor(1,0,0,1)
    d_right:drawObject()
    graphics.setDrawColor()
    d_right.flash = d_right.flash - 1
  else
    d_right:drawObject()
  end

  --[[
  if attacker == d_left then
    d_left.scale:set(d_left.scale.x + 0.01, d_left.scale.y + 0.01)
    d_right.scale:set(1,1)
  else
    d_right.scale:set(d_right.scale.x + 0.01, d_right.scale.y + 0.01)
    d_left.scale:set(1,1)
  end
  --]]
end

function script:renderDragon()
  dragon:drawObject()
  self:_renderHeads()
end

function script:renderLife()
  if dragon_life <= 0 then
    dragon.state = "win"
  end

  dragon_life = d_left.life + d_right.life

  graphics.drawRect(320 - 200, 20, 400, 12, color.GREY)
  graphics.drawRect(320 - 200, 22, dragon_life, 8, 0, 1, 0, 1)

  graphics.label("ENERGY:" .. p1charge, 40, 420)
  graphics.label("SHIELD:" .. life, 40, 440)
end

function script:restartGame()
  dragon.attack_delay1  = realTime + 5
  dragon.recover        = realTime + 999
  dragon_life           = 400
  d_left.life, d_right.life = 200, 200
  dragon.pos:set(dragon.original_pos.x, dragon.original_pos.y)

  attacker = d_left
  ship.scale:set(3.0, 3.0)
  ship.hit = 0
  ship.immuneUntil = 0

  for i, v in ipairs(title) do v.display = true end

  yscroll = 0
  cZone   = 1
  script:generateCave()
  dragon.state = "title"
end

function script:generateCave()
  local w, h

  if platform == "LOVE" then
    w, h = 16, 16
  else
    w, h = 0.0625, 0.25
  end

  cavegrid = {}
  for y=1, 11 do
    cavegrid[y] = {}
    for x=1, 14 do
      cavegrid[y][x] = script:getTiles(cZone)
    end
  end

  cave.uv[3], cave.uv[4] = w, h
  --cave.uv[5], cave.uv[6] = 16, 16
  cave.size:set(16,16)
end

function script:renderCave()
  local w, h
  if platform == "LOVE" then
    w, h = 16, 16
  else
    w, h = 0.0625, 0.25
  end


  yscroll = yscroll + 0.2
  if yscroll > 48 then
    table.remove(cavegrid, 1)
    table.insert(cavegrid, {})
    for x=1, 14 do
        table.insert(cavegrid[#cavegrid], script:getTiles(cZone))
    end
    yscroll = 0
  end

  graphics.push()
  graphics.translate(0, yscroll)
  graphics.setDrawColor(0.6,0.6,0.6,1)
  graphics.startBatch(cave.texture)
  if platform == "LOVE" then
    for y, yv in ipairs(cavegrid) do
      for x, xv in ipairs(yv) do
        cave:setUV(xv[1], xv[2], w, h)
        cave.pos.x, cave.pos.y = (x-1) * 48, 456 - (y-1) * 48
        graphics.addToBatch(cave)
      end
    end
  end

  if platform == "DC" then
    local cave = cave
    for y, yv in ipairs(cavegrid) do
      for x, xv in ipairs(yv) do
        cave.uv[1] = xv[1]
        cave.uv[2] = xv[2]
        cave.pos.x, cave.pos.y = (x-1) * 48, 456 - (y-1) * 48
        graphics.addToBatch(cave)
      end
    end
  end
  graphics.endBatch(cave.texture)
  graphics.pop()
end

function script:titleScreen()
  weather.setClearColor(0,0,0,1)
  graphics.drawRect(0,0,640,480,0,0,0,1)

  local a = math.abs(math.sin(realTime * 10))
  --title[8]:drawObject()
  title[1]:drawObject()

  graphics.setDrawColor(1,1,1,a)
  title[2]:drawObject()
  graphics.setDrawColor()

  title[3]:drawObject()
  title[4]:drawObject()
  title[5]:drawObject()
  title[6]:drawObject()
  title[7]:drawObject()
end

function script:getTiles(zone)
  local w, h
  if platform == "LOVE" then
    w, h = 16, 16
  else
    w, h = 0.0625, 0.25
  end

  if      zone == 1 then
    return {math.random(9,14)*w, math.random(2)*h}
  elseif  zone == 2 then
    return {}
  end
end

function script:switchTo(name)
  if dragon.state ~= name then
    if      name == "intro" then
      for i, v in ipairs(title) do v.display = false end
      flux.to(ship.pos,   5, {x=320,y=400}):onstart(
      function()
        dialog.setFile(currentMap.desc_file)
        local text = dialog.getText()
      end)

      flux.to(ship.scale, 0.7, {x=1.5,y=1.5}):delay(5):oncomplete(
      function()

      end)

      flux.to(dragon.pos, 1, {x = 320, y = 100}):delay(6):oncomplete(
      function()
        script:switchTo("game")
      end)
    elseif  name == "game" then
      p1:setPosition(ship.pos.x, ship.pos.y)
      ship.pos = p1.obj.pos
    end

    dragon.state = name
  end
end

function script:_containPlayer()
  local p1x, p1y = p1.obj.pos.x, p1.obj.pos.y

  if p1y > 470 then
    p1.obj.pos.y = 470
  end

  if p1y < 350 then
    p1.obj.pos.y = 350
  end
end


return script
