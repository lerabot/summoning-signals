local script = script:new()
local map

local garage_door

function script:onLoad()
  map = self.parent

  p1:pickItem("Repair Kit")

  local door_text = map:getObject("garage_door_text")
  door_text.display = false
  door_text.canRepair = 1

  garage_door = map:getObject("garage_door")

  event:register("open_garage", function()
    garage_door.pos.y = garage_door.pos.y - 12
  end)


end

function script:activate()
end

function script:desactivate()
end

function script:update()
end

function script:render()
end

return script
