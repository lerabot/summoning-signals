local script = script:new()

script.currentPage = 1
script.maxPage = 7
script.page = {}

local logo

function script:onLoad()
  p1.obj.display = false
end


function script:activate()
  logo = gameObject:createFromFile("asset/map_guide/POH.png", 220, 40)
  self.parent:addObject(logo)
  p1.obj.display = false
end

function script:desactivate()
  graphics.freeTexture(logo.texture)
end

function script:update()
  local p = script.currentPage

  if      p1:getButton("LEFT") then
    p = math.max(p - 1, 1)
  elseif  p1:getButton("RIGHT") then
    p = math.min(p + 1, self.maxPage)
  end

  script.currentPage = p
end

function script:_renderPage()
  local p = script.currentPage

  graphics.setDrawColor(0,0,0,1)
  --graphics.print("-- PILOT OPERATING HANDBOOK --", 40, 20)
  if self.page[p] ~= nil then
    local s = "   (".. p .. "/ " .. self.maxPage .. ") " .. script.page[p].title
    graphics.print(s, 35, 55)
    for i, v in ipairs(script.page[p].content) do
      graphics.print(v, 50, 35 + 50 * i)
    end
  else
    graphics.setDrawColor(color.ERROR)
    local s = "   (".. p .. "/ " .. self.maxPage .. ") MEMORY CORRUPTED"
    graphics.print(s, 35, 55)
    --for i, v in ipairs(script.page[p].content) do
      --graphics.print(v, 150, 60 * i)
    --end
  end
end

function script:render()
  graphics.setClearColor(color.GREY)
  self:_renderPage()
end



-- Signaling
script.page[1] = {
  title = "HARDWARE",
  content = {
    "Line 1",
    "Line 2",
    "Line 3",
    "Line 4",
    "Line 5",
  },
}

-- Radio
script.page[4] = {
  title = "RADIO",
  content = {
    "AM - FM : Signal encoding type operating on different frequency band",
    "SIG : Toggle the radio ON/OFF",
  },
}

-- Repairs
script.page[7] = {
  title = "REPAIRS",
  content = {
    "Every pilot need to be able to do some ship repairs. Here are some basic facts :",
    "ZIP-TIES : Flexible and easy to install. Wont last forever.",
    "SCREW : Solid mechanical attachement. Can be undone.",
    "WELDING : Sturdy. Lasts forever.",
  },
}

return script
