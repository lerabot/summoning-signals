local script = script:new()
local maf = require "lib.maf"
local gameObject = require "gameobject"

local water
local shadow = {}

local bup, bdown
local boat1, b1_shadow
local boatActive = false
local dest = {}
local index, nextStop = 1


local p = "M 835.49022,467.93763 759.06165,799.36621 627.63308,1040.7948 293.34736,1125.0805 780.49022,1315.0805 1333.3474,1422.2234 1830.8717,1449.156 1633.9289,1352.3249 1599.6708,1095.1227 1915.9798,418.07822 1207.9029,583.50834 Z"

local map

function script:onLoad()
  map = self.parent
  shadow[1] = map:getObject("shadow1")
  shadow[2] = gameObject:copy(map:getObject("quai2"))
  shadow[2].scale:set(-1,-1)
  shadow[2].pos:set(shadow[2].pos.x, shadow[2].pos.y + shadow[2].size.y*0.4)

  boat1 = map:getObject("boat")
  boat1.canRepair = 1
  boat1.npcID = "boat1"
  --table.insert(map.objects, boat1)

  portal = map:getObject("portal")
  map:removeObject("portal")
  map:addTrigger(portal)

  for w in string.gmatch(p, "%S+") do
    local x = string.match(w, "%a+")
    if x then
    else
      local x, y = string.match(w, "(%-?%d+.%d+),(%-?%d+.%d+)")
      --print(x .. " " .. y)
      if x == nil or y == nil then
        print("asdfsd")
      end
      table.insert(dest, maf.vector(tonumber(x), tonumber(y)))
    end
  end

  event:register("toggle_boat", function()
    boatActive = not boatActive

    if boatActive == false then
      graphics.setCamTarget(p1.obj)
      p1.obj.active = true
    else
      --graphics.setCamTarget(boat1)
      p1.obj.active = false
    end

    if index == 1 then nextStop = 4 end
    if index == 4 then nextStop = 7 end
    if index == 7 then nextStop = 1 end

    print("Boat status : " .. tostring(boatActive))
  end)

end

function script:activate()
  water = gameObject:createFromFile("asset/map_harbour/water.png")

  boat1.pos:set(dest[1].x, dest[1].y)
  if p1:hasQuest(QUEST_DRAGON) ~= false or nil then
    boat1.active = true
    boat1.display = true
  else
    boat1.active = false
    boat1.display = false
  end

  boat1.active = true
  boat1.display = true

  for i, v in ipairs(shadow) do
    v.display = false
  end

  self.parent.drawObjectFlag = false
end

function script:desactivate()
  water:delete()
end

function script:update()
  weather.setClearColor(0,0,1,1)
  --table.insert(map.drawable, boat1)

  if boatActive then
    self:_updateBoat()
  end

end

function script:render()

  graphics.setDrawColor(0.2, 0.2, 0.2, 0.3)
  for i, v in ipairs(shadow) do
    v.display = true
    v:drawObject()
    v.display = false
  end

  local c = draw_color
  graphics.setDrawColor(c[1]*0.5,c[2]*0.5,c[3]*0.5,0.6)
  portal:drawObject()
  script:_renderWater()

  graphics.setDrawColor(c)
  self.parent.drawObjectFlag = true
  self.parent:render()
  self.parent.drawObjectFlag = false

  --graphics.drawRect(boat1.pos.x, boat1.pos.y, 20, 20, 1,0,0,1)
end

function script:_renderWater()
  local s, c = math.sin(realTime)*7.0, math.cos(realTime/2.0)*3.0
  local step = water.scale.x * water.size.x

  graphics.setTransparency(0.6)
  local col = draw_color
  graphics.setDrawColor(col[1]*0.5,col[2]*0.5,col[3]*0.5,0.6)
  graphics.startBatch(water.texture)

  local p1x, p1y = p1.obj.pos.x, p1.obj.pos.y
  for x=-100.0, self.parent.width + 200, step do
    for y =-100.0, self.parent.height + 200, step do
      if sh4_distance(p1x, p1y, x, y) < 750 then
        water.pos:set(x+s, y+c)
        --water:drawObject(x+s, y+c)
        graphics.addToBatch(water)
      end
    end
  end

  graphics.endBatch(water.texture)
  graphics.setDrawColor(1,1,1,1)
  graphics.setTransparency(1)
end

function script:_updateBoat()

  local d = dest[index]
  --d = p1.obj.pos
  local arrived = boat1:moveTo(d, 0.1)
  if arrived then
    if index == nextStop then
      event:emit("toggle_boat")
      return nil
    end

    index = index + 1
    if index > #dest then index = 1 end
  end

  boat1:updatePosition()
  --boat1.pos.y = boat1.pos.y + math.sin(frameCount*0.3) * 0.1
  p1.obj.pos:set(boat1.pos.x, boat1.pos.y)


  if      boat1.vel.x > 0 and boat1.vel.y > 0 then
    boat1:setSprite("claude_BR.png", currentMap.texture, currentMap.spriteData, 0)
  elseif  boat1.vel.x > 0 and boat1.vel.y < 0 then
    boat1:setSprite("claude_TR.png", currentMap.texture, currentMap.spriteData, 0)
  elseif  boat1.vel.x < 0 and boat1.vel.y > 0 then
    boat1:setSprite("claude_BL.png", currentMap.texture, currentMap.spriteData, 0)
  elseif  boat1.vel.x < 0 and boat1.vel.y < 0 then
    boat1:setSprite("claude_TL.png", currentMap.texture, currentMap.spriteData, 0)

  end
  --[[
  if boat1.vel.x > 0 then
    boat1.scale:set(1, 1)
  else
    boat1.scale:set(-1, 1)
  end
  if boat1.vel.y < 0 then
    boat1:setTexture(bup)
  else
    boat1:setTexture(bdown)
  end
  --]]


end

function script:_renderBoat()


end

return script
