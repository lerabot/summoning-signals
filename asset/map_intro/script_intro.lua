local script = script:new()

local isPlayed, setCam = false, false
local v_intro

function script:onLoad()
  p1:setVisible(false)
  hw.display = false
  v_intro = video.load(findFile("asset/video/intro.ogv"))
  --graphic.setCamTarget(nil)
  --graphics.setCamPosition(320, 240)
end

function script:activate()
end

function script:update()
  if isPlayed == false then
    video.play(v_intro)
  end

  if video.isDone(v_intro) then
    currentMap:switch("crash")
  end
end

function script:render()
  graphics.setDrawColor(1,1,1,1)
  video.render(v_intro)

end

function script:desactivate()
  local startX, startY = 450, 360
  p1:setPosition(startX, startY)
  graphics.setCamPosition(startX, startY)
end

return script
