--[[
Arcade title at  http://arcade.photonstorm.com
Font = Gyruss (Konami)

]]

local script = script:new()

local arcade, arcadePos
local startArcade = nil

function script:onLoad()

  arcade = self.parent:getObject("arcade")
  -- wierd Dreamcast work around because caps:removeObject doesn't work on DC
  --[[
  arcadePos = maf.vector(arcade.pos.x, arcade.pos.y)
  arcade.pos.x = 1000
  arcade = gameObject:copy(arcade)
  arcade.pos = arcadePos
  table.insert(self.parent.npcs, arcade)
  --]]
  --self.parent:removeObject("arcade")

  p1:pickItem("KNOT token")

  -- add a sfx here
  event:register("toggle_dragon_arcade", function()
    startArcade = realTime + 4
  end)

end

function script:activate()
end

function script:desactivate()
end

function script:update()
  if startArcade and startArcade < realTime then
    currentMap:switch("dragon")
    startArcade = nil
  end

  weather.setClearColor(0,0,0,1)
  weather.setDrawColor(1,1,1,1)
end

function script:render()

  local rand = 0.5 + (math.random(10) * 0.05)
  graphics.setDrawColor(1, 1, 1, rand)
  graphics.setTransparency(rand)
  arcade:drawObject()
  graphics.setDrawColor()
  graphics.setTransparency(1.0)
end

return script
