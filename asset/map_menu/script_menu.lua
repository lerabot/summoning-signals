local script = script:new()

local options = {
  {"Start", function() audio.stop(currentMap.bgm) gameworld.newGame() end},
  {"Load",  function() script.mode = "Load" end},
  {"Teleport", function() script.mode = "Teleport" end},
  {"Exit",  function() script:exit() end},
}

local currentMap
local currentMenu = {}
local selection, mIndex = 1, 1
local mOffset = 0
local c2      = 200 --column 2 position

local fullname, bg
local loadedGame,loadData = false, nil

function script:onLoad()
  currentMenu = options
  currentMap  = self.parent

  script.animTime = realTime
  currentMap.drawObjectFlag = false
  currentMap.bgm = audio.load("bgm/title_screen.ogg", "stream")
  currentMap.bgm.volume = 10

  if 1 == 1 then
    local vmu = {"Load found VMU", function()
      local t = "DATA CORRUPTED"
      local y = graphics.getTextWidth(t)
      graphics.label_delay(t, 320, 240, color.ERROR)
    end}

    --table.insert(options, 3, vmu)
  end

  script.mode = ""
end

function script:activate()
  fullname = gameObject:createFromFile("asset/map_menu/fullname2.png", 320, 240)
  bg = gameObject:createFromFile("asset/map_menu/bg.png", 450, 240)

  weather.lightActive(false)
  currentMap.playerState = "menu"
  p1:setVisible(false)
  hw.display = false
  audio.play(currentMap.bgm, 1, true)
end

function script:desactivate()
  audio.stop(currentMap.bgm)
  weather.lightActive(true)

  bg:delete()
  fullname:delete()

  p1:setVisible(true)
  hw.display = true
end

function script:update()
  weather.setClearColor(color.GREY)

  --bg.pos.y = 240 +  math.sin(realTime * 0.5) * 5

  if script.mode == "" then
    if p1:getButton("DOWN") then
      selection = selection + 1
    elseif p1:getButton("UP") then
      selection = selection - 1
    end
    if p1:getButton("A") or p1:getButton("RIGHT") then currentMenu[selection][2]() end
    if p1:getButton("B") or p1:getButton("LEFT") then script.mode = "" end
    selection = math.min(math.max(selection, 1), #currentMenu)

  elseif script.mode == "Teleport" then
    if      p1:getButton("UP") then
      if mIndex + mOffset < 6 then
        mIndex = math.max(1, mIndex - 1)
      else
        mOffset = math.max(mOffset - 1, 0)
      end
    elseif  p1:getButton("DOWN") then
      if mIndex + mOffset > 4 then
        mOffset = math.min(mOffset + 1, #MAP_NAMES - 5)
      else
        mIndex  = math.min(mIndex + 1 , 5)
      end
    elseif  p1:getButton("A") or p1:getButton("RIGHT") then
      currentMap:switch(MAP_NAMES[mIndex + mOffset])
    elseif  p1:getButton("B") or p1:getButton("LEFT") then
      script.mode = ""
    end

  elseif script.mode == "Load" then
    if      p1:getButton("UP") then
      mIndex = math.max(1, mIndex - 1)
    elseif  p1:getButton("DOWN") then
      mIndex = math.min(mIndex + 1 , #loadData)
    elseif  p1:getButton("A") or p1:getButton("RIGHT") then
      local result = saveload:load(mIndex)
      if result ~= 0 then
        saveload:applyLoad()
      else
        audio.play(s)
      end
    elseif  p1:getButton("B") or p1:getButton("LEFT") then
      script.mode = ""
    end
  end
end

function script:render()
  local c = copy(color.ACTIVE)
  c = {c[1],c[2],math.random(5, 10) * 0.1, 1}

  graphics.setClearColor(color.GREY)
  graphics.setDrawColor(color.ACTIVE)
  bg:drawObject(bg.pos.x - 5, bg.pos.y+2)
  graphics.setDrawColor()
  --bg:drawObject()

  graphics.setDrawColor(c[1],c[2], c[3], 1)
  graphics.setTransparency(0.95)
  fullname:drawObject()
  graphics.setTransparency()


  graphics.setDrawColor()
  for i,v in ipairs(currentMenu) do
    local t = v[1]
    if selection == i then
      graphics.setDrawColor(c[1], c[2], c[3],1)
    else
      graphics.setDrawColor(0.3,0.3,0.3,1)
    end
    graphics.print(t, 70, 260 + (i * 20))
  end
  graphics.setDrawColor()

  if script.mode == "About" then
    script:about()
  elseif script.mode == "Load" then
    script:loadGame()
  elseif script.mode == "Teleport" then
    script:teleport()
  elseif script.mode == "" then
  end
end

function script:loadGame()
  if loadedGame == false then
    saveload.updateSaveInfo()
    loadData = saveload.getSaveInfo()
    loadedGame = true
  else
    for i, v in ipairs(loadData) do
      if v.map == "" then
      else
        c = color.LGREY
        if mIndex == i then
          --graphics.label("<< Load.", 480,   80 + (i-1) * 30, color.ACTIVE)
          c = color.ACTIVE
        end
        local s = "G" .. i .. " > " .. v.map .. " " .. math.floor(v.time/60) .. " mins"
        graphics.print(s, c2, 260 + (i * 20), c)
      end
    end
  end
end

function script:teleport()
  for i = 1, 5 do
    local m = MAP_NAMES[i + mOffset]
    local c = color.LGREY
    if mIndex == i then
      c = color.ACTIVE
    end
    graphics.print(m, c2, 260 + (i * 20), c)
  end
end

function script:about()
  local y = 280
  --[[
  graphics.print("This project wouldn't be\npossible without :", 320, y)
  graphics.print("Kazade / Mrneo240 / Rizzo", 320, y + 40)
  graphics.print("Protofall / Simulant Discord", 320, y + 60)
  --]]
  --[[
  graphics.print("A warm thank you to our patrons :", 320, 150)
  patrons = require "patrons"
  for i,v in ipairs(patrons) do
    graphics.print("- " .. v, 320, 150 + (i * 20))
  end
  --]]
end

function script:exit()
  if platform == "LOVE" then
    love.event.quit(exitstatus)
  end
end



return script
