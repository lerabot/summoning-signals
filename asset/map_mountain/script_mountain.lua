local script = script:new()

local agave1, agave2, agave3
local panel1

function script:onLoad()
  map = self.parent

  panel1 = map:getObject("panel1")
  panel1.canRepair = ITEM_BROKEN

  agave1 = map:getObject("agave1")
  agave2 = map:getObject("agave2")
  agave3 = map:getObject("agave3")

  agave1:addAnimation("agave", {1,2,3,4}, "pingpong")
  agave2:addAnimation("agave", {1,2,3,4}, "pingpong")
  agave3:addAnimation("agave", {1,2,3,4}, "pingpong")

  map.bgm = audio.load("bgm/roches_keys.ogg", "stream")

  p1:setPosition(320, 240)
end


function script:update()
  if panel1.canRepair == 2 and p1.currentQuest == QUEST_BATTERY then
    p1.currentQuest = QUEST_COPPER_END
  end

  agave1:updateAnimation(25)
  agave2:updateAnimation(20)
  agave3:updateAnimation(22)

end

return script
