local script = script:new()

function script:onLoad()
  self.parent.bgm = audio.load("bgm/reine.ogg", "stream")
end

function script:activate()
end

function script:desactivate()
end

function script:update()
end

function script:render()
end

return script
