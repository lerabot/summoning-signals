local script = script:new()
local map

local destination = {
}

local buttons = {
  {"Journal",     function() end},
  {"Controls",    function() end},
  {"Save",        function() end},
  {"Load",        function() end},
}

local sIndex, selected = 1, nil
local mIndex, maxMenu = 1, 1
local cursor

local repairText = ""
local saveData = nil

script.nextMap = nil

function script:onLoad()
  map = self.parent
  --map.activate = function() self:activate() end

  self.button = {}
  local save = gameObject:new()
  save.npcID = "Save"
  save.size:set(60, 20)
  save.pos:set(70, 400)
  save.action = function() saveload.save() end

  local load = gameObject:new()
  load.npcID = "Load"
  load.size:set(60, 20)
  load.pos:set(70, 425)
  load.action = function() saveload.load() end

  table.insert(self.button, save)
  table.insert(self.button, load)

  cursor = gameObject:copy(p1.obj)

  self.animTime = 0
  self.switch = false

  event:register("reset_menu_position", function()
    sIndex = 1
  end)

  event:register("update_save_data", function()
    saveload.updateSaveInfo()
    saveData = saveload.getSaveInfo()
  end)

  event:register("update_repair_info", function()
    script:repairInfo()
  end)
end

function script:update()
  if p1:getButton("LEFT") then
    sIndex = math.max(1, sIndex - 1)
    mIndex = 1
  elseif p1:getButton("RIGHT") then
    sIndex = math.min(sIndex + 1 , #buttons)
    mIndex = 1
  elseif p1:getButton("UP") then
    mIndex = math.max(1, mIndex - 1)
  elseif p1:getButton("DOWN") then
    mIndex = math.min(mIndex + 1 , maxMenu)
  end

  if p1:getButton("A") then
    buttons[sIndex][2]()
  end

  -- Only check for the save/VMU data if needed
  if sIndex > 2 and saveData == nil then
    event:emit("update_save_data")
  elseif sIndex <= 2 then
    saveData = nil
  end
end

function script:render()
  script:drawBox()
  if      sIndex == 1 then
    script:journal()
  elseif  sIndex == 2 then
    script:controlInfo()
  elseif  sIndex == 3 then
    script:save()
  elseif  sIndex == 4 then
    script:load()
  end


  local label, offset = "", 0
  for i, v in ipairs(buttons) do
    label = v[1]
    offset = graphics.getTextWidth(label) * 0.5
    if i == sIndex then
      graphics.drawRect(50 + (i * 100) - 45, 418, 90, 24, 0,0,0,1)
      graphics.print(label, 50 + (i *100) - offset, 420, color.ACTIVE)
    else
      graphics.drawRect(50 + (i * 100) - 45, 418, 90, 24, 0,0,0,1)
      graphics.print(label, 50 + (i *100) - offset, 420)
    end
  end
end

function script:activate(mapName)
  if  mapName ~= "overworld" then
    self.nextMap = mapName
  end
  self.switch = true
  self.animTime = realTime + 1
end

function script:drawBox()
  local bW, bH = 540, 380
  --graphics.drawRect(320-bW*0.5, 240-bH*0.5, bW, bH, 1,1,1,0.9)
  --bW = bW - 2
  --bH = bH - 2
  graphics.drawRect(320-bW*0.5, 240-bH*0.5, bW, bH, 0,0,0,0.7)
end

function script:journal()
  local y, space = 50, 20

  graphics.label("Ship repairs:", 45, y + space * 0)
  graphics.print(repairText, 60, y + space * 1.3)

  graphics.label("Quest log:", 45, y + space * 5)

  local i, desc = 0, ""
  for k, v in pairs(p1.quests) do
    graphics.label(v.name, 45, y + space * (6 + i), color.ACTIVE)
    i = i + 1
    graphics.label(v.desc, 60, y + space * (6 + i))
    i = i + 1
  end
end

function script:save()
  local info = saveData
  local c = color.WHITE
  maxMenu = #info

  graphics.label("Save file:", 52, 50)
  for i, v in ipairs(info) do
    c = color.WHITE
    if mIndex == i then
      graphics.label(">> Save.", 480,   80 + (i-1) * 30, color.ACTIVE)
      c = color.ACTIVE
    end
    graphics.print("File " .. i .. " : ", 60,   80 + (i-1) * 30, c)
    graphics.print(v.map,                 150,  80 + (i-1) * 30, c)
    graphics.print(math.floor(v.time/60) .. " mins",                300,  80 + (i-1) * 30, c)
  end

  if p1:getButton("A") then
    saveload:save(mIndex)
    saveload.updateSaveInfo(mIndex)
    saveData = saveload:getSaveInfo()
  end
end

function script:load()
  local info = saveData
  local c = color.WHITE
  maxMenu = #info

  graphics.label("Load file:", 52, 50)
  for i, v in ipairs(info) do
    c = color.WHITE
    if mIndex == i then
      graphics.label("<< Load.", 480,   80 + (i-1) * 30, color.ACTIVE)
      c = color.ACTIVE
    end
    graphics.print("File " .. i .. " : ", 60,   80 + (i-1) * 30, c)
    graphics.print(v.map,                 150,  80 + (i-1) * 30, c)
    graphics.print(math.floor(v.time/60) .. " mins",                300,  80 + (i-1) * 30, c)

  end

  if p1:getButton("A") then
    saveload:load(mIndex)
    saveload:applyLoad()
    p1:toggleOverworld()
  end
end

function script:repairInfo()
  local info = {}
  local done = "[X]"
  local inProgress = "[ ]"

  if p1:hasQuest(QUEST_HET) == Q_DONE then
    table.insert(info, "[X] Repair HE thrusters.")
  else
    table.insert(info, "[ ] Repair HE thrusters.")
  end

  if p1:hasQuest(QUEST_XENON) == Q_DONE then
    table.insert(info, "[X] Find Xenon gas.")
  else

    table.insert(info, "[ ] Find Xenon gas.")
  end

  if p1:hasQuest(QUEST_MICROPROCESSOR) == Q_DONE then
    table.insert(info, "[X] Salvage DSI Microprocessor.")
  else
    table.insert(info, "[ ] Salvage DSI Microprocessor.")
  end

  repairText = table.concat(info, "\n")
end

function script:controlInfo()
  local info = {}
  if      platform == "LOVE" then
    info[1] = "ARROW    : Cursor Movement"
    info[2] = "SPACEBAR : Interact"
    info[3] = "B        : Cancel / Back"
    info[4] = "X        : Toggle Inventory"
    info[5] = "ENTER    : Toggle Menu"

    graphics.label("Controls:", 52, 50)
    graphics.print(table.concat(info, "\n"), 60, 75)
  elseif  platform == "DC" then

  end
end

return script
