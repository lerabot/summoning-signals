local script = script:new()
script.name = "Pannel / Game intro"

local played = false
local map
local ogRender
local bigTV
local animTime = 10

function script:onLoad()
  map = self.parent.parent
  ogRender = copy(self.parent.render)
  self.parent.render = self.render

  local tv = map:getObject("tv")
  bigTV = gameObject:copy(tv)
  bigTV.pos:set(320 - bigTV.size.x/2, -100)
end

function script:update(deltaTime)
  local deltaTime = deltaTime or 0
  if played == false then
    animTime = animTime - deltaTime
    if animTime < 0 then played = true end

    if bigTV.pos.y < 240 then
      bigTV.pos.y = bigTV.pos.y + 1
    end
  end

  if played == true then
    self.parent.render = ogRender
  end
end

function script:render()
  if played == false then
    bigTV:drawObject()
  end
end


return script
