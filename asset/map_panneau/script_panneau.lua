--[[
Harvest rocks - copper -
]]--

local hw        = require "hardware"
local script    = script:new()

local sd --sprite data
local panelNum = 6
local panels = {}

local map = {}

local sfx = {
  active_sfx = nil,
  pannel_elec = {}
}

-- MAIN FUNCTIONS---------------------------
function script:onLoad()

  sd  = self.parent.spriteData
  map = self.parent
  map.mode = "basic"
  map.drawObjectFlag = true

  -- Sync the hardware panels with
  for i, v in ipairs(hw.panels) do
    if hw.panels[i].obj == nil then
      hw.panels[i].obj = map:getObject("panneau0" .. i)
    end
  end

  -- SOUNDS LOADING
  --[[
  for i=1, 3 do
    sfx.pannel_elec[i] = audio.load("/asset/sounds/fx/panneau_elec_" .. i .. ".ogg")
  end
  --]]
  return 1
end

function script:update(deltaTime)

  for i, v in ipairs(hw.panels) do
    updatePanneau(v)
  end
end

function script:render()
  --if true then return nil end

  for i, v in ipairs(hw.panels) do
    drawPanneau(v)
  end

  graphics.setDrawColor(1.0, 1.0, 1.0, 1.0)

  --data/world--------------------------------------
  --renderScripts(self)
end

--PANNEAU LOGIC---------------------------
function updatePanneau(panel, dt)
  local obj = panel.obj

      --[[
      hw.chargeBattery(obj.efficiency)
      if realTime > obj.animTime then
        if obj.frame == 8 or obj.frame == 1 then obj.direction = -obj.direction end
        obj.frame     = obj.frame + obj.direction
        obj.uv       = {getTextureData(sd, "panneau000" .. obj.frame .. ".png")}
        obj.quad:setViewport(unpack(obj.uv))
        obj.animTime  = realTime + math.random(0.2, 0.5)
      end
      --]]

  --[[
  if obj.status == "charging" then
    --math.clamp(obj.scale.x * getSunDir(), 0, 5)
    local oritentation = obj.scale.x * getSunDir() * 0.3
    obj.efficiency = math.max(0.1, 1 + climate + oritentation)
    hw.chargeBattery(obj.efficiency)
    if realTime > obj.animTime then
      if obj.frame == 8 or obj.frame == 1 then obj.direction = -obj.direction end
      obj.frame     = obj.frame + obj.direction
      obj.uv       = {getTextureData(sd, "panneau000" .. obj.frame .. ".png")}
      obj.texture:setViewport(unpack(obj.uv))
      obj.animTime  = realTime + math.random(0.2, 0.5)
    end
  end

  if obj.status == "upgrading" then
    obj.status_time = obj.status_time - dt
    if obj.status_time < 0 then
      upgradeDone()
      obj.status = "normal"
    end
  end

  if obj.status == "broken" then
    obj.active = -1
  end
  --]]

  -- NEEDS TO BE IN CURRENT MAP! ------------------
  --if mode == "log" or currentMap ~= maps.panneau then return end

  if p1:press(obj, 'A') then

    obj.scale.x = -obj.scale.x
    --_playPanSwitch()
  end

  --_playPanelNoise()

end

local panelAnimation = function(panel)
  local obj = panel.obj -- LOL
  if realTime > obj.anim.time then
    if      obj.anim.cFrame == 8 then obj.anim.direction = -1
    elseif  obj.anim.cFrame == 1 then obj.anim.direction =  1 end
    obj.anim.cFrame     = obj.anim.cFrame + obj.anim.direction
    obj.uv              = {getTextureData(sd, "panneau000" .. obj.anim.cFrame .. ".png")}
    if platform == "LOVE" then
      obj.quad:setViewport(unpack(obj.uv))
    end
    obj.anim.time        = realTime + math.random(0.2, 0.5)
  end
end

function drawPanneau(panel)
  if panel.status == "upgrading" then
    --graphics.setDrawColor(1, 0, 0, 1)
    --obj:drawObject()
    --setDrawColor()
    return
  end

  if panel.status == "online"  then
    activeColor()
    panelAnimation(panel)
    panel.obj:drawObject()
    return
  end

  if panel.status == "onfline" then
    setDrawColor()
    panel.obj:drawObject()
    return
  end
end



function _playPanelNoise()
  local active = false
  local choice = 1

  for i, v in ipairs(panels) do
    if p1:isOver(v) and v.active == 1 then
      choice = math.ceil(v.efficiency)
      --print(choice .. " > " .. v.efficiency)
      active = true
      break
    end
  end

  if active == false and audio.isPlaying(sfx.active_sfx) then
    audio.stop(sfx.active_sfx)
    sfx.active_sfx = nil
  end

  if active == true and sfx.active_sfx == nil then
    sfx.active_sfx = sfx.pannel_elec[choice]
    audio.setVolume(sfx.active_sfx, 0.5)
    audio.play(sfx.active_sfx, 1)
  end
end

function _playPanSwitch()
  if platform == "LOVE" then
    local intensity = climate
    local sfx1 = audio.load("/asset/sounds/fx/panneau_low_" .. math.random(1,6) .. ".ogg", "stream")
    local sfx2 = audio.load("/asset/sounds/fx/panneau_high_" .. math.random(1,6) .. ".ogg", "stream")
    audio.play(sfx1)
    audio.play(sfx2)
  end
end



function printPanStat(obj, i)
  local lines = "**************"
  local maxL = math.ceil(obj.efficiency * 4)
  local speed = math.abs(2 - obj.efficiency) * 10
  local num = math.ceil(frameCount/speed % maxL)
  local l = string.sub(lines, 1, num)
  --print(speed)
  local state = "online"
  if obj.active == -1 then
    state = "offline"
    l = ""
  end
  if obj.status == "upgrading" then
    state = "upgrading"
    graphics.setDrawColor(1,0,0,1)
  end
  if obj.status == "broken" then
    state = "broken"
    graphics.setDrawColor(0.7,0,0,1)
  end

  local s = "P" .. i .. "_" .. state .. "\n  <" .. l .. ">"
  graphics.print(s, obj.obj.pos.x + obj.obj.size.x/2, obj.obj.pos.y)


  console.add(cons, "temp")
end


return script
