local script = script:new()

local map
local queen, bg
local a = false
local intro = nil

function script:onLoad()
  map = self.parent
  queen = map:getObject("queen")

  if p1:hasQuest(QUEST_QUEEN) == false then
    intro = video.load("asset/video/queen.ogv")
  else
    intro = nil
  end

  --p1:setVisible(false)

  bg    = gameObject:createFromFile("asset/map_queen/bg.png", 320,240)
  bg.scale:set(1.35, 1)

  queen = gameObject:createFromFile("asset/map_queen/q2_red_dc.png", 320, 240)
  queen:setNpc("queen")
  map:addObject(queen)
  queen.scale:set(0.7, 0.7)
end

function script:activate()
  hw.display = false
  --p1:inspectRoutine(queen)
end

function script:desactivate()

end

function script:update()
  if intro then
    video.play(intro)
    if video.isDone(intro) == false then
      p1:inspectRoutine(queen)
      intro = nil
    end
  end



  weather.setClearColor(1, 0,0,1)
  weather.setDrawColor(1,1,1,1)
end

function script:render()

  bg:drawObject()

  graphics.setDrawColor(1.0, 0, 0, 1)
  graphics.setTransparency(0.3)
  queen.scale:set(0.80, 0.75)
  queen:drawObject(queen.pos.x, queen.pos.y-20)

  graphics.setTransparency(1.0)
  graphics.setDrawColor(1, 0.8, 0.8, 1.0)
  queen.scale:set(0.7, 0.75)
  queen:drawObject(queen.pos.x, queen.pos.y)
  graphics.setDrawColor()

  if intro then
    video.render(intro)
  end

end

return script
