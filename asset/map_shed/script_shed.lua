local script = script:new()

local key, drawKey
local map
local cat
function script:onLoad()
  map = self.parent

  cat = gameObject:createFromFile("asset/default/cat.png", 440, 610)
  cat:setNpc("cat")
  map:addObject(cat)

  key = self.parent:getObject("shed_key")
  local shed_door = self.parent:getObject("shed_door")

  map.shedItem = {}
  table.insert(map.shedItem, itemList.find("FUZZ-EEL Fuse"))
  table.insert(map.shedItem, itemList.find("12v battery (empty)"))

  event:register("getItemFromShed", function()
    local item = table.remove(map.shedItem, math.random(#map.shedItem))
    if item ~= nil then
      p1:pickItem(item)
      return true
    else
      return false
    end
  end)
end

function script:update()

end

function script:render()
  weather.setClearColor(0.85,0.85,0.85,1)

  if weather.getSunPosition() < -0.0 then
    local glow = (math.sin(realTime * 2) + 1) * 0.2
    graphics.setDrawColor(glow, glow, glow, 1)
    key:drawObject()
    graphics.setDrawColor()
  end
end

return script
