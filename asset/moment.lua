local moment = {}

moment.active = false
moment.mode = ""
moment.lastMode = ""
moment.lastUntil = 0

function moment.bigText(string, type, delay)
  local center = (graphics.width/2.0) - (#string * 8.0 / 2.0)
  local y = (graphics.heigth / 2) - (graphics.textSize /2)

  if moment.active == false then
    moment.lastUntil = realTime + delay
    moment.active = true
  end

  if moment.active == true then
    p1.active = false
    graphics.setDrawColor(1,1,1,1)
    --graphics.setFont("big")
    graphics.print(string, center, y)
  end

  if moment.active == true and realTime > moment.lastUntil then
    moment.active = false
  end

end

function moment.clear()
  p1.active = true
  graphics.setFont()
end

function moment.isActive()
  return moment.active
end

return moment
