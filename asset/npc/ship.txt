# ship

  That was a close one...
  [Press Spacebar to continue]

  What happened up there?
  Never heard anything like this on the radio...

  And that labyrinth?
  All just happened so fast...

  Well the ship is a complete disaster now.
  |

  Looks like the thruster is busted,
  I'm lucky the Xenon tank has not blown up.

  Let's check the radio,
  Maybe there is someone around to help?

  ! graphics.addTooltip("Use the arrows to move around\nPress X to toggle inventory", 20, 20, 7) hw.display = true

  > radio
=========================

# radio

  ? p1:hasQuest(QUEST_COPPER) == 3
  > radio

  Let's check the solar panel,
  I'll come back to the ship later.
  -
  Let's check the radio first,
  I can't get in there anyway.

=============================
