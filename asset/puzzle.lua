local tiles = require "puzzle_tiles"

local puzzle = {}
puzzle.__index = puzzle
setmetatable(puzzle, {__call = function(cls, ...) return csl.new(...) end,})

local TILE_TYPES = {}
local CURSOR_ID = 100

function puzzle.init()
  TILE_TYPES[CURSOR_ID] = tiles.cursor

  TILE_TYPES[0] = tiles.empty
  TILE_TYPES[1] = tiles.red
  TILE_TYPES[2] = tiles.blue
  TILE_TYPES[3] = tiles.green
end

function puzzle:new(sizeX, sizeY, _initial, _solution)
  local _puzzle = {
    size      = {x = sizeX, y = sizeY},
    cursor    = {x = 1, y = 1},
    initial   = _initial,
    current   = _initial,
    solution  = _solution, -- sparse array of tileID
    selected  = nil,       --if a tile is selected
    highlight = nil,
  }
  local self = setmetatable(_puzzle, puzzle)
  return self
end

function puzzle:update()
  local osc = math.sin(os.clock() * 50) + 1
  TILE_TYPES[CURSOR_ID].c = {osc, 1, 1, 0.5}


  local c = self.cursor

  if self.selected == nil then
    if input.getButton("LEFT") then
      c.x = math.max(c.x - 1, 1)
    end
    if input.getButton("RIGHT") then
      c.x = math.min(c.x + 1, self.size.x)
    end
    if input.getButton("UP") then
      c.y = math.max(c.y - 1, 1)
    end
    if input.getButton("DOWN") then
      c.y = math.min(c.y + 1, self.size.y)
    end
    if input.getButton("A") then
      self.selected = copy(self.cursor)
      self.highlight = copy(self.cursor)
    end
  else
    if input.getButton("LEFT") then
      self.highlight.x = math.max(self.cursor.x - 1, 1)
    end
    if input.getButton("RIGHT") then
      self.highlight.x = math.min(self.cursor.x + 1, self.size.x)
    end
    if input.getButton("UP") then
      self.highlight.y = math.max(self.cursor.y - 1, 1)
    end
    if input.getButton("DOWN") then
      self.highlight.y = math.min(self.cursor.y + 1, self.size.y)
    end
    if input.getButton("B") then
      self.selected = nil
      self.highlight = nil
    end
    if input.getButton("A") then
      self:swapTile(self.selected, self.highlight)
    end

  end

  self.cursor = c
end

function puzzle:render()
  local tSize = 40
  local tSpacing = tSize + 2
  local tColor = {1,0,0,1}
  local y = 0

  local tileID, t = 0

  graphics.push()
  graphics.translate(320 - (tSpacing * self.size.x * 0.5), 250 - (tSpacing * self.size.y * 0.5))
  -- draw puzzle
  for y=1, self.size.y do
    for x=1, self.size.x do
      tileID = self.current[x + ((y-1) * 5)]
      t = TILE_TYPES[tileID]
      graphics.drawRect((x-1) * tSpacing, (y-1) * tSpacing, tSize, tSize, table.unpack(t.c))
    end
  end

  --draw cursor
  graphics.drawRect((self.cursor.x-1) * tSpacing, (self.cursor.y-1) * tSpacing, tSize, tSize, table.unpack(TILE_TYPES[CURSOR_ID].c))


  if self.selected then

  end

  if self.highlight then
    graphics.drawRect((self.highlight.x-1) * tSpacing, (self.highlight.y-1) * tSpacing, tSize/2, tSize/2, 0.3, 0.3, 1, 1)
  end
    graphics.pop()
end

function puzzle:swapTile(s, h)
  local _s = self:getTileNumber(s)
  local _h = self:getTileNumber(h)

  if _h ~= nil then
    local _temp = self.current[_s]
    self.current[_s] = self.current[_h]
    self.current[_h] = self.current[_temp]
  end
end

function puzzle:getTileNumber(_tile)
  local n = _tile.x + (_tile.y * self.size.x)
  if n < 0 or n > self.size.x * self.size.y then
    return nil
  else
    return n
  end
end

function puzzle:savePosition()
  table.save()
end
return puzzle
