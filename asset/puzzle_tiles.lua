local tiles = {}

tiles.empty = {
  c = {0,0,0,1},
  f = function() end,
}

tiles.red = {
  c = {1,0,0,1},
  f = function() end,
}

tiles.blue = {
  c = {0,0,1,1},
  f = function() end,
}

tiles.green = {
  c = {0,1,0,1},
  f = function() end,
}

tiles.cursor = {
  c = {0,1,1,1},
  f = function() end,
}

return tiles
