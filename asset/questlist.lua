local quests = {}

NO_QUEST      = 0
Q_START       = 1
Q_ACTIVE      = 1
Q_DONE        = -1
Q_FAIL        = -2

-- CHAPTER 1
QUEST_CRASH     = 1
QUEST_COPPER    = 2
QUEST_CAT       = 4
QUEST_FLARE     = 3
QUEST_ROPER     = 5
QUEST_QUEEN     = 6

-- CHAPTER 2
QUEST_FIXBOAT   = 21
QUEST_HARBOUR   = 20
QUEST_DRAGON    = 22

-- CHAPTER 3
QUEST_ALCHEMIST = 50

-- ENDGAME
QUEST_HET                 = 101
QUEST_XENON               = 102
QUEST_MICROPROCESSOR      = 103

quests.crash = {
  id    = QUEST_CRASH,
  name  = "Crashlanding",
  file  = "crash",
  desc  = "Where are we?",
  progress = 1,
}

quests.copper = {
  id    = QUEST_COPPER,
  file  = "copper",
  name  = "Meeting Copper",
  desc  = "A guy named Copper will meet me\nat the solar panels.",
}

quests.cat = {
  id    = QUEST_CAT,
  file  = "cat",
  name  = "Moonlight Cat",
  desc  = "Met a strange cat near in the woods.",
}

quests.harbour = {
  id    = QUEST_HARBOUR,
  file  = nil,
  name  = "Explore Coral harbour",
  desc  = "Explore the harbour area.",
}

quests.fixboat = {
  id    = QUEST_FIXBOAT,
  file  = nil,
  name  = "Claude's fuse",
  desc  = "Claude needs a fuse for his boat's motor.",
}

quests.roper = {
  id    = QUEST_ROPER,
  file  = nil,
  name  = "The roper.",
  desc  = "The workers will only let me pass\nif I fix their broken roper.",
}

quests.dragon = {
  id    = QUEST_DRAGON,
  file  = nil,
  name  = "Bring me the head",
  desc  = "I need to find the dragon and\nbring one of its head.",
}

quests.alchemist = {
  id    = QUEST_ALCHEMIST,
  file  = nil,
  name  = "Captive?",
  desc  = "---",
}

quests.het = {
  id    = QUEST_HET,
  name  = "",
  desc  = "",
}

quests.xenon = {
  id    = QUEST_XENON,
  name  = "",
  desc  = "",
}

quests.microprocessor = {
  id    = QUEST_MICROPROCESSOR,
  name  = "",
  desc  = "",
}


return quests
