local repair = script:new()
local gameObject = require "LANTERN_ENGINE.gameobject"

local t = {}
local gear

-- GLOBALS FOR STATUS
ITEM_REPAIRED = 100
ITEM_BROKEN   = 1



function repair:init()
  --t = repair:generate(nil)
  gear = gameObject:createFromFile("asset/icon/mechanic.png", 0, 0)
  gear.scale:set(0.5, 0.5)
end

function repair:generate(obj)
  local diff   = diff or 5
  local d      = 10 - diff
  local soluce = {-50 + math.random(-d, d) , -50 + math.random(-d, d),
                  50 + math.random(-d, d)  , -50 + math.random(-d, d),
                  0 + math.random(-d, d)   ,  50 + math.random(-d, d)
                }

  local template = {
    obj     = obj,
    soluce  = soluce,
    diff    = diff,
    s_angle = math.random(0, 31416 * 2),
    o_angle = 1.3,

    pattern = function(self)
      --[[
      if (frameCount % 120 - (diff * 10) == 0) then
        self.s_angle = self.s_angle + math.random(-3 * diff, 3 * diff)
        self.s_angle = self.s_angle % 31416
        print(self.s_angle)
      end
      --]]

      self.s_angle = self.o_angle + math.sin(realTime) * (1000 * diff)


      --self.s_angle = self.s_angle + (diff * 25)

      t.s_angle = t.s_angle % (31416 * 2)
    end,

    sync = 0.0,
    sync_state = false,

    poly = copy(soluce),
    p_angle = math.random(0, 31416 * 2),
    p_force = 0.0,
    p_vel   = 0.0,
    p_accel = 0.0,
  }
  t = template
  print("REPAIR> Generated repair for " .. tostring(obj.npcID))
  return template
end

function repair:update()
  local c = input.getController()

  if c.buttonPressed["RIGHT"] then
    t.p_force = t.p_force + 15
  end
  if c.buttonPressed["LEFT"] then
    t.p_force = t.p_force - 15
  end

  t.p_vel   = t.p_vel + t.p_force
  t.p_angle = t.p_angle + t.p_vel
  t.p_vel   = t.p_vel * 0.95
  t.p_force = 0
  t.p_angle = t.p_angle % (31416 * 2)

  t:pattern()
  --local result = 0
  local result = repair:compareTri(t)

  -- is the objects is repaired
  if result == true then
    t.obj.canRepair = ITEM_REPAIRED
  end

  return result
end

function repair:render()
  if p1.state ~= state.repair then return nil end

  if t.sync ~= 1 then
    --[[
    graphics.push()
    graphics.translate(320, 240)
    --graphics.rotate(t.s_angle * 0.00004)
    gear:drawObject(t.soluce[1] + 10, t.soluce[2] -3)
    gear:drawObject(t.soluce[3]- 2, t.soluce[4] + 4)
    gear:drawObject(t.soluce[5], t.soluce[6])
    graphics.pop()
    --]]
    graphics.push()
    graphics.translate(320, 240)
    graphics.rotate(t.p_angle * 0.0001)
    graphics.drawPoly(t.poly, 0.1, 0, 0.3 + (t.sync * 0.6), 0.8)
    graphics.pop()

    graphics.push()
    graphics.translate(320, 240)
    graphics.rotate(t.s_angle * 0.0001)
    --graphics.drawPoly(t.soluce, 0.8, 0, 0, 0.2)
    --gear:drawObject(t.soluce[1], t.soluce[2])
    --gear:drawObject(t.soluce[3], t.soluce[4])
    --gear:drawObject(t.soluce[5], t.soluce[6])
    repair._tri(t.soluce[1], t.soluce[2], 3.1416)
    repair._tri(t.soluce[3], t.soluce[4], 1.068144)
    repair._tri(t.soluce[5], t.soluce[6], -1.005312)
    graphics.pop()

    local text = string.format("Sync:%2.0f%%", t.sync * 100)
    if t.sync_state == true then
      graphics.label(text, 320 - graphics.getTextWidth(text)/2, 320, color.ACTIVE)
    else
      graphics.label(text, 320 - graphics.getTextWidth(text)/2, 320, color.WHITE)
    end

  else
    graphics.push()
    graphics.translate(320, 240)
    local s = math.random(7,13) * 0.1
    graphics.scale(s, s, 1)
    graphics.rotate(t.p_angle * 0.0001)
    graphics.drawPoly(t.poly, color.ACTIVE)
    graphics.pop()
    local text = "Repair done."
    graphics.label(text, 320 - graphics.getTextWidth(text)/2, 240, color.ACTIVE)
  end
end

function repair:getRepair()
  return t
end

function repair:compareTri(puz)

  --print(math.abs(puz.s_angle - puz.p_angle))

  if t.sync < 1 then
    local v = math.abs(puz.s_angle - puz.p_angle)
    if(v < 800 - (puz.diff * 30)) then
      t.sync_state = true
      t.sync = math.min(t.sync + 0.005, 1.0)
    else
      t.sync_state = false
      t.sync = math.max(t.sync - 0.002, 0.0)
    end
    t.timer = 100

  elseif t.sync >= 1 then
    self.pattern = function() end

    t.timer = t.timer - 1
    if t.timer == 0 then
        return true
    else
        return false
    end

  end

  return false
end

function repair._tri(x, y, a)
  local s = 5
  local poly = {
    -s, -s, s, -s, 0, s
  }

  if platform == "DC" then
    --y = -y
  end

  local c = color.ACTIVE
  graphics.push()
  --graphics.origin()
  graphics.translate(x, y, 0)
  graphics.rotate(a)

  graphics.drawPoly(poly, c[1], c[2], c[3],1)
  graphics.pop()
end

return repair
