local maf   = require "lib.maf"

local signal = {
  available = false,
  generator = nil,
  message   = "", -- the actual message output
  filtered  = "",
  distance  = 0,
  content   = {}, -- the message as a array of ID
  emitter   = {}, -- the emitter of the current signal
}

local emitters = {
}

local DIST3 = 200
local DIST2 = 100
local DIST1 = 30

local allMessage = {}

local sounds = {}
local currentSound = 1
local delayUntilNextWord = 0
local nextStringChop = 0
local nextTVsfx = 0
local nextSFX = {}
local sfx = {}
local nextSignalRefresh = 0

function signal.init(realTime)
  math.randomseed(os.time())
  --signal.mainSubject = lume.randomchoice(lex.subjects)
  --print("SIGNAL> Main subjects = " .. signal.mainSubject)

  -- SOUNDS
  --local tv_start = audio.load("asset/sounds/fx/battery_start_2sec.ogg")
  --sfx.tv_stop  = audio.load("asset/sounds/fx/battery_stop_1.ogg")
  --sfx.tv_loop1 = audio.load("asset/sounds/fx/battery_loop.ogg")
  --nextSFX = sfx.tv_start

  -- Emits
  copper = gameObject:new("", 320, 0)

  log.add("Signal init.") print("Signal> Init done.")
end

function signal.addEmitter(emit)
  if emit ~= nil then
    table.insert(emitters, emit)
  end
end

function signal.updateAfterLoad()
  for i,v in ipairs(emitters) do
    v:updateAfterLoad()
  end
end

-- Simple way to see if the signal is ready for diffusion
function signal.isAvailable()
  return signal.available
end

-- Search for a signal based on frequency proximity
function signal.find(frequency)
  local f = frequency
  local closest, closestD = nil, DIST3

  for i, v in ipairs(emitters) do
    local pos = v.pos
    local d = f:distance(pos)
    if d < closestD and d < v.difficulty and v.encoding == hw.encoding then
      closest, closestD = v, d
    end
  end

  -- check if closest exist and is ready for next transmission
  if closest ~= nil and closest.nextTransmission < realTime then
    --signal.available  = true
    signal.emitter    = closest
    signal.distance   = closestD
    return true
  else
    --signal.available  = false
    signal.emitter    = nil
    signal.distance   = 9999
    signal.message    = nil
    return false
  end
end

-- Continually updates the founds signal
function signal.update()
    -- If no capsule, encapsultate
    if signal.generator == nil then
      dialog.setFile(signal.emitter.desc, signal.emitter.desc_position)
      local block = dialog.getText(signal.emitter)
      signal.generator = nil
    end

    -- Run the whole message is a signal is active
    if signal.generator then
      signal.message = signal.generator()
      signal.available = true
    end

    if signal.message == nil then
      signal.message = ""
      signal.generator = nil
      signal.available = false
    end
end

--[[
-- This create the capsule for the signal // SHOULD MOVE TO EMMITTOR ???
function signal.encapsulate()
  local cLine       = 1
  local emit        = signal.emitter
  local message     = signal.emitter.messages[emit.progress]
  local nextMessage = realTime + signal.emitter.refreshRate

  return function()
    local m = message[cLine]

    -- Check if the next message should come in
    if realTime > nextMessage then
      cLine = cLine + 1
      nextMessage = realTime + signal.emitter.refreshRate
    end

    -- If the line isn't empty
    if m ~= nil then
      return m

    -- If the line is empty
    else
      emit.progress = emit.progress + 1
      emit.nextTransmission = realTime + emit.delay
      return nil
    end
  end
end
--]]

-- Filter the regular output based on distance and frequency
function signal.filterMessage()
  local m = signal.message
  local fm = ""
  local disto = math.floor(signal.distance)


  for i=1, #signal.content do
    local w = signal.content[i]
    if disto > 10 then
      fm = fm .. string.rep(".", math.random(4))
      fm = fm .. w:sub(math.random(#w), math.random(#w)) -- pas le best
      fm = fm .. string.rep(".", math.random(4))
    else
      fm = fm .. w
    end
    fm = fm .. " "
  end
  signal.filtered = fm

  return signal.filtered
end

-- Filter based on encoding ???
function signal.filterEncoding()
  if signal.emitter.encoding ~= hw.encoding then
    local c = 0
    local c_array = {}
    local fm = signal.filtered

    for i=1, #fm do
      if      fm:byte(i) == 46 then
        c = 46
      elseif  i % 3 == 1 then
        c = fm:byte(math.random(97, 122))
      elseif  i % 3 == 2 then
        c = fm:byte(math.random(97, 122))
      elseif  i % 3 == 3 then
        c = fm:byte(math.random(97, 122))
      else
        c = fm:byte(i)
      end
      table.insert(c_array, c)
    end

    fm = string.char(unpack(c_array))
    return fm
  else
    return signal.filtered
  end
end

-- THIS RETURNS THE CURRENT LINE OF MESSAGES
function signal.getMessage(tag)

end

-- DEPRECATED : Generate a message (randomly)
function signal.generateMessage()
  if signal.available == false then signal.content = "" end

  signal.content = {}
  local l = 1 + math.random(2)
  local emit_content = signal.emitter[4]

  for i=1, l do
    table.insert(signal.content, lume.randomchoice(emit_content))
  end

  signal.message = table.concat(signal.content, " ")
  --print("ORIGINAL : " ..signal.message)
  return signal.message
end

-------------------------------------

--should free the smaple files here
function _resetAudioSignal()
  currentSound = 0
  sounds = {}
  delayUntilNextWord = realTime
end

function _addToSound(message, word)
  local word = string.lower(word)
  local disto_level = 0

  if climate < 0 then
    disto_level = -climate * 3
    disto_level = math.ceil(disto_level)
    --print("SIGNAL>disto level = " .. disto_level)
  end

  local path = "/asset/sounds/words/disto_" .. disto_level .. "/" .. word .. " 1.ogg"
  local sample = audio.load(path, "static")

  if sample ~= nil then
    table.insert(sounds[message], sample)
  end
end

function _playChords()
  local path = "/asset/sounds/input_chords/input_chords1_" .. math.random(1, 22) .. ".ogg"
  local chord = audio.load(path, "stream")
  audio.setVolume(chord, 0.4)
  audio.play(chord)
end

function _playStringChop()
  if realTime > nextStringChop then
    local path = "/asset/sounds/strings_chop1/strings_chop1_" .. math.random(1, 18) .. ".ogg"
    local chord = audio.load(path, "stream")
    audio.setVolume(chord, 0.5)
    audio.play(chord)
    nextStringChop = realTime + 1
  end
end

function _playTVsfx()
    --[[
    if audio.isPlaying(sfx.tv_start) then nextSFX = sfx.tv_loop1 audio.setLoop(sfx.tv_loop1, true) end
    if audio.isPlaying(sfx.tv_loop1) then nextSFX = sfx.tv_stop end

    if nextTVsfx < realTime then
      audio.play(nextSFX)
      nextTVsfx = audio.getDuration(nextSFX) + realTime
      if audio.isPlaying(sfx.tv_start) then nextTVsfx = 2 + realTime end
    end
    --]]
end

function _stopTVsfx()
  --[[
  if audio.isPlaying(sfx.tv_loop1) or audio.isPlaying(sfx.tv_start) then
    audio.stop(sfx.tv_loop1)
    audio.stop(sfx.tv_start)
    audio.play(sfx.tv_stop)
    nextTVsfx = realTime
  end
  --]]
end

function _playMessage(sequence)
  local i = currentSound
  local cMess = cMess - 1

  if realTime < delayUntilNextWord then return end
  if sounds[cMess] == nil then return end

  -- checks if no sound is playing
  local sample = sounds[cMess][i]
  if audio.isPlaying(sample) ~= true then
    -- if current sound is fine, plays it
    if sample ~= nil then audio.play(sample) end

    -- increment the current sound
    currentSound = currentSound + 1
    local nextSample = sounds[cMess][currentSound]
    -- checks if the next sound is not nil
    if nextSample ~= nil then
      delayUntilNextWord = realTime + audio.getDuration(nextSample) + 0.25
    else
      delayUntilNextWord = realTime + 0.4
    end
  end
end

function filterSignal(message, noise)
  --0, 1 range
  local noise = noise or 0
  local filteredMessage = message or "INVALID"
  local quality = system.textQuality + (climate/4) + noise

  if quality < 0.4 then
    filteredMessage = "[.. unable to decode..]"
  end

  return filteredMessage
end

function printAllMessage()
  drainBattery(5) --between 20/30 ?
  local x = 100
  graphics.print("Day " .. getDay() .. "", 40, 40)
  --graphics.print("LOG:", 100, 440)
  for i=1, 25  do
    if allMessage[i] ~= nil then
     graphics.print(allMessage[i], 100 + 40, 20 + (i * 20))
    end
   end
end

function getMessageCount()
  return #allMessage
end

function getSubjects()
  return subjects
end

function getMainSubject()
  return mainSubject[1]
end

function removeMainSubject()
  for i, v in ipairs (subjects) do
    if v == mainSubject[1] then
      table.remove(subjects, i)
      print(v .. " left forever")
    end
  end
end

function changeMainSubject()
  mainSubject[1] = lume.randomchoice(subjects)
  print("Main subjects = " .. mainSubject[1])
end

return signal
