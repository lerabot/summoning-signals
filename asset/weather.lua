local graphics = require "graphics"
local weather = {
}

local sunPos, lastSun, sunDir, sun  = 0, 0, 0, 0
local weatherTypes    = {"Sunny", "Cloudy", "Thunder", "Rainy"}
local currentWeather  = "Thunder"
local weatherPeriod   = 0
local ambiance = nil

-- RAIN
local rainIntensity = 100
local rainDirection = maf.vector(math.random(-3, 3), 10)

-- CLOUDS
local cloudNum = 7

-- THUNDER
local t_strikes = 4
local t_delay = 0

local c1, r --original game object for texture
local clouds = {}
local rain = {}

BG_color = {1,1,1,1}      -- Sky/BG Color
BG_overwrite = {0,0,0,0}
draw_color = {1,1,1,1}    -- Object color based on "sky"
active_color = {1,1,1,1}  -- Flashing color

function weather.init()

  if platform == "LOVE" then
    c1 = gameObject:createFromFile("asset/romdisk/cloud.png", 100, 100)
    for i = 1, cloudNum do
      local c = gameObject:copy(c1)
      c.pos:set(math.random(0, 1000), math.random(0, 200))
      local sX = math.random(10, 20) / 5
      c.scale:set(sX, sX)
      table.insert(clouds, c)
    end

    r = gameObject:createFromFile("asset/romdisk/rain.png", 100, 100)
    for i = 1, 300 do
      local c = gameObject:copy(r)
      c.pos:set(math.random(-400, 1400), math.random(-1000, -100))
      local sX = math.random(10, 20) / 30
      c.scale:set(sX, sX)
      --c.angle = rainDirection:angle(maf.vector(0, -1)) / 360
      table.insert(rain, c)
    end
  end

  weather.lightStatus = true

  --currentWeather = _getNewWeather()
  --print("CLIMATE> Current Weather = " .. currentWeather)
end

function weather.update(dt)
  weather.updateTime(dt)

  if weather.lightStatus then
    weather.updateLights()
  end
  --weather.updateWeather(dt)


  --if weatherPeriod < realTime then
    --currentWeather = _getNewWeather()
    --print("CLIMATE> " .. currentWeather .. " until " .. weatherPeriod )
  --end
end

function weather.render(type)
  --graphics.print("SUN:" .. weather.getSunPosition(), 20, 20)

  graphics.setClearColor(BG_color)
  --graphics.drawRect(0,0,640, 480, BG_color[1], BG_color[2], BG_color[3], 1)
  graphics.setDrawColor(draw_color)

  --if moment.active then return end
  --[[
  if mode == "log" then return end

  if type == "under" then
    if currentMap.name == "fort" then
      if currentWeather == "Cloudy" then _cloudRender() end
    end

  elseif type == "over" then
    if      currentMap.name == "panneau" then
      if currentWeather == "Cloudy" then _cloudRender() end
      if currentWeather == "Rainy" then _rainRender() end
    elseif  currentMap.name == "fort" then
      if currentWeather == "Rainy" then _rainRender() end
    end
  end
  --]]
end

function weather.delete()
  r:delete()
  c1:delete()

  for i,v in ipairs(clouds) do v:delete("NOASSET") end
  for i,v in ipairs(rain) do v:delete("NOASSET") end
end

function weather.lightActive(state)
  local state = state or false
  weather.lightStatus = state
end

function weather.updateLights()
  sunPos = weather.getSunPosition("range")

  BG_color = {
    sunPos * 0.6 + 0.1,
    sunPos * 0.6 + 0.1,
    0.75 - sunPos * 0.4,
    1.0
  }

  local c = math.min(0.2 + sunPos * 0.8, 1.0)
  draw_color = {
    c,
    c,
    c,
    1.0
  }


  local a = math.abs(math.sin(realTime * 7)) * 0.2 + 0.8
  active_color = {a,a,a,a}

end

function weather.updateTime(dt)
  realTime    = tonumber(realTime + dt)
  frameCount  = frameCount + 1
  days        = getDay()
  lastSun     = sun
  sun         = weather.getSunPosition()
end

function weather.updateWeather(dt)

  if      currentWeather == "Sunny" then
  elseif  currentWeather == "Cloudy" then
    _cloud()
  elseif  currentWeather == "Thunder" then
    _thunderStorm()
  elseif  currentWeather == "Rainy" then
    _rain()
  end
end

function weather.setClearColor(r, g, b, a)
  if type(r) == table then
    BG_color = copy(r)
  else
    BG_color = {r, g, b, a}
  end
end

function weather.setDrawColor(r, g, b, a)
  draw_color = {r, g, b, a}
end

function _thunderStorm()
  if realTime > t_delay and t_strikes > 0 then
    BG_color = {1,1,1,1}
    t_delay = realTime + math.random(4, 12)/50
    t_strikes = t_strikes - 1
    if t_strikes == 1 then
      local sfx = audio.load("/asset/sounds/fx/thunder_stuff_" .. math.random(1, 3) .. ".ogg", "static")
      --local sfx = audio.load("/asset/sounds/fx/neg" .. math.random(1, 4) .. ".ogg", "static")
      --audio.setVolume(sfx, math.random(0.2, 0.5))
      audio.play(sfx)

    end
  end

  if t_strikes == 0 then
    t_delay = realTime + math.random(15, 30)
    t_strikes = math.random(2,4)
    if climate < 0.3 then t_strikes = math.random(4, 7) end
  end
end

function _rain()
  local v = {}
  for i=1, rainIntensity do
    v = rain[i]
    v.pos:add(rainDirection)
    if v.pos.y + (v.size.y * v.scale.y) > 480 and fadeWeather() == false then
       v.pos.y = -100 v.pos.x = math.random(1000)
     end
  end
end

function _rainRender()
  graphics.setDrawColor(1,1,1,1)
  local v = {}
  for i=1, rainIntensity do
    v = rain[i]
    v:drawObject()
  end
  graphics.setDrawColor(draw_color)
end

function _cloud()
  for i, v in ipairs(clouds) do
    v.pos.x = v.pos.x - (1 / v.scale.x)
    if v.pos.x + (v.size.x * v.scale.x) < -10 then v.pos.x = 1100 end
  end
end

function _cloudRender()
  graphics.setDrawColor(1,1,1,0.5)
  for i, v in ipairs(clouds) do
    v:drawObject()
  end
  graphics.setDrawColor(draw_color)
end

function _getNewPeriod()
  return realTime + dayLenght/3 + math.random(dayLenght/3, dayLenght)
end

function _getNewWeather(choice)
   local w = choice or lume.randomchoice(weatherTypes)

   if audio.isPlaying(ambiance) then audio.stop(ambiance) end


   if w == "Sunny" then

   elseif w == "Rainy" then
     for i, v in ipairs(rain) do v.pos:set(math.random(-400, 1400), math.random(-1000, -100)) end
     rainDirection = maf.vector(math.random(-2, 2), 15)
     rainIntensity = math.random(120, 200)
     if climate < 0 then
       local mod = -climate
       rainIntensity = rainIntensity + (mod * 100)
     end
     --ambiance = audio.load("/asset/sounds/fx/rain_loop_2.ogg", "stream")
     --audio.play(ambiance, 1)
     --audio.setVolume(ambiance, 0.6)
   elseif w == "Cloudy" then

   elseif w == "Thunder" then
   end

   weatherPeriod = _getNewPeriod()
   return w
end

function fadeWeather()
  if weatherPeriod - 7 < realTime then
    if currentWeather == "Rainy" then
      --rainIntensity = rainIntensity - 10
    end
    return true
  end
  return false
end

function weather.getSunPosition(mode)
    -- -1 to 1 range
  if      mode == nil then
    return math.cos((realTime + timeOffset) * mainSpeed)

  -- 0 to 1 range
  elseif  mode == "range" then
    return math.cos((realTime + timeOffset) * mainSpeed) * 0.5 + 0.5
  end
end

function weather.getSunOritentation()
  local dir = sun - lastSun
  if dir < 0 then return 1
  else return -1
  end
end

function weather.isDay()
  if weather.getSunPosition() > 0 then
    return true
  else
    return false
  end
end

function weather.isNight()
  return not weather.isDay()
end


function getDay()     return math.floor(realTime/(dayLenght*2)) end
function getSeconds() return realTime end
function newDay()     if days ~= lastDay then return 1 else return nil end end

function setBGColor()   graphics.setClearColor(BG_color) end
function setDrawColor() graphics.setDrawColor(draw_color)   end
function activeColor()  graphics.setDrawColor(active_color)   end

return weather
