function love.conf(t)
  --t.window.width = 640
  --t.window.height = 480
  t.window.title = "Summoning Signals"
  --t.identity = nil                    -- The name of the save directory (string)
  --t.appendidentity = false            -- Search files in source directory before save directory (boolean)
  t.window.borderless = false
  t.version = "11.1"
  t.release = "2.2"

  t.window.fullscreen = true
  --t.window.x = 100
  --t.window.y = 100
end
