local globals     = require "globals"
local gameworld   = require "gameworld"

function gameworld.create(startMap)
  if platform == "DC" then
    os.clock = C_clock
  end


  local t = os.clock()
  print(t)
  local r = 0
  -- SH4 SINE
  for i=1, 100000 do
    r = sh4_sin(1)
  end
  local t1 = os.clock()
  local message = string.format("100k SH4 SINE: %.10f", t1 - t)
  print(message)

  -- REGULAR SINE
  t = os.clock()
  for i=1, 100000 do
    r = math.sin(1)
  end
  t1 = os.clock()
  local message = string.format("100k REGULAR SINE: %.10f", t1 - t)
  print(message)

  return 1
end

function gameworld.update(dt)
  return 1
end

function gameworld.render()
  graphics.setClearColor(0, 0, 1, 1)
  return 1
end

function gameworld.free()
  return 1
end

return gameworld
