local globals     = require "globals"
--local profiler    = require "lib.profile"

local obj = {}
local tri = {-50, -50, 50, -50, 0, 50}
local a = 0.01
local catNum = 1
local frameCount = 0

function gameworld.create(startMap)
  obj[1] = gameObject:createFromFile("asset/default/cat.png", 320, 240)
  obj[2] = gameObject:createFromFile("asset/default/spacemono.png", 320, 240)

  obj[1].scale:set(2,2)
  return 1
end

function gameworld.update(dt)
  --graphics.setClearColor(0,0,0,1)
  deltaTime = dt
  input.update()

  if input.getButton("A") then
    catNum = catNum + 10
  end

  return 1
end

function gameworld.render(dt)
  graphics.setClearColor(0.3,0.3,0.3, 1)


  local a = math.abs(math.sin(frameCount*0.1))
  frameCount = frameCount + 0.1

  graphics.drawRect(10, 10, 620, 460, 0.6, 0, 0, 1)

  graphics.setDrawColor(1,0,1, a)
  obj[1]:drawObject(280, 240)
  graphics.setDrawColor(1,0,1, a)
  obj[1]:drawObject(300, 240)
  graphics.drawRect(300, 190, 100, 100, 0, 1, 1, a)

  graphics.endFrame(true)
  --graphics.label(table.concat(info, "\n"), 20, 20);
  return 1
end

function gameworld.free()

  return 1
end

return gameworld
