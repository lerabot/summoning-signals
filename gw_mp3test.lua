local globals     = require "globals"
--local profiler    = require "lib.profile"

local frameCount = 0
local song

function gameworld.create(startMap)
  song = audio.load("bgm/reine.ogg", "stream")
  audio.play(song)
  return 1
end

function gameworld.update(dt)
  deltaTime = dt
  input.update()

  return 1
end

function gameworld.render(dt)
  graphics.endFrame(true)
  return 1
end

function gameworld.free()

  return 1
end

return gameworld
