globals     = require "globals"
gameworld   = require "gameworld"
player      = require "player"

local tri = {}
local v0, v1, v2
local max, min = math.max, math.min
local lPosition
local outside, f

function gameworld.create(startMap)
  print(os.clock())

  p1 = player:new()
  p1:setPosition(100, 100)
  lPosition = p1:getPosition("vector")

  v0 = maf.vector(50, 50)
  v1 = maf.vector(150, 50)
  v2 = maf.vector(350, 250)
  v3 = maf.vector(50, 150)
  v4 = maf.vector(50, 300)

  triangle = {v0, v1, v2}
  triangle1 = {v0, v3, v2}
  triangle2 = {v4, v3, v2}
  t1 = {v0.x, v0.y, v1.x, v1.y, v2.x, v2.y}
  t2 = {v0.x, v0.y, v3.x, v3.y, v2.x, v2.y}

  allTri = {triangle,triangle1, triangle2}

  local s = 0
  s = os.clock()
  for i=1,1000 do
    collision.LUA_insideTriangle(p1.obj.pos, triangle)
  end
  print("LUA : 1k Point Inside Triangle function: " .. os.clock() - s)

  s = os.clock()
  for i=1,1000 do
    collision.pointInTri(p1.obj.pos, triangle)
  end
  print("SH4 : 1k Point Inside Triangle function: " .. os.clock() - s)
  return 1


end


local checkDist = 15
local bounceForce = 1
function gameworld.update(dt)
  input.update()









  coll, tNum = collision.check(p1:getDirection(checkDist, true), allTri)
  if coll then
    outside = false
  elseif outside == false then
    outside = true
    f = p1:getDirection(bounceForce)
  end

  if outside then
    outside = false
    p1:resetDynamic()
    p1:addForce(-f)
    p1:addForce(-p1.cont.joy)
  end

  p1:updatePlayer()
  return 1
end

function gameworld.render()

  --[[
  if collision.pointInTri(p1.obj.pos, triangle) then
    graphics.setDrawColor(0,0,1,1)
  else
    graphics.setDrawColor(1,0,0,1)
  end
  graphics.drawPoly(t1)
  --]]
  for i,v in ipairs(allTri) do
    if i == tNum then
      graphics.drawPoly(v, 0, 1, 0, 1)
    else
      graphics.drawPoly(v, 1, 0, 0, 1)
    end
  end


  p1:render()
  local d = p1:getDirection(checkDist, true)
  local p = p1:getPosition("vector")
  graphics.setDrawColor()
  --p1.obj:drawObject(d.x, d.y)

  return 1
end

function gameworld.free()

  return 1
end


return gameworld
