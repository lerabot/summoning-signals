require "platform"

local gameworld = {}
local canevas = {}
local savenum = nil

function love.load(arg)
  if arg ~= nil then
    love.conf = nil
    startmap = arg[1] or nil
    savenum = arg[2] or nil
    if arg[2] == "true" then ss_debug = true end
  else
  end
  love.graphics.setDefaultFilter("nearest", "nearest", 16)
  --love.graphics.setDefaultFilter("linear", "linear", 4)
  --love.keyboard.setKeyRepeat(false)
  if startmap == "release" then
    love.window.setFullscreen(true)
    startmap = "menu"
  end
  love.mouse.setVisible(false)

  love.window.setFullscreen(true)

  initPlatform()
  gameworld = loadGameworld("gw_summoning.lua")
  gameworld.init()
  gameworld.create(startmap, savenum)
end

function love.update(dt)
  gameworld.update(dt)
end

function love.draw()
  local sx, sy = 0, 0
  local tx, ty = 0, 0
  --scale to dreamcast size
  local nativex, nativey = graphics.getNativeSize()
  local windowx, windowy = graphics.getWindowSize()
  sx, sy = math.floor(windowx / nativex), math.floor(windowy / nativey)
  --sx, sy = (windowx / nativex), (windowy / nativey)

  if windowx > nativex then tx = (windowx - (nativex * sy)) / 2 end
  if windowy > nativey then ty = (windowy - (nativey * sy)) / 2 end


  -- Draw the game
  love.graphics.setCanvas(canvas)
  love.graphics.setBlendMode("alpha")
  love.graphics.clear()
  gameworld.render()

  love.graphics.setCanvas()
  love.graphics.setBlendMode("replace")
  love.graphics.setColor(color.GREY)
  love.graphics.rectangle("fill", 0, 0, windowx, windowy)

  love.graphics.translate(tx, ty, 0)
  love.graphics.scale(sy, sy)
  love.graphics.setColor(1,1,1,1)
  love.graphics.draw(canvas)

  love.graphics.origin()
  if debug == true then
    graphics.print("P1:X" .. math.floor(p1.obj.pos.x) .. " Y" .. math.floor(p1.obj.pos.y), 20, 20)
  end

  -- Fullscreen stuff
end

function conf_debug(t)
  t.window.width = 640
  t.window.height = 480
  t.window.title = "Summoning Signals - DEV"
  t.window.fullscreen = false
  --t.identity = nil                    -- The name of the save directory (string)
  --t.appendidentity = false            -- Search files in source directory before save directory (boolean)
  t.window.borderless = false
  t.version = "11.1"
  t.release = "2.2"

end

function conf_release(t)
  --t.window.width = 640
  --t.window.height = 480
  t.window.title = "Summoning Signals"
  --t.identity = nil                    -- The name of the save directory (string)
  --t.appendidentity = false            -- Search files in source directory before save directory (boolean)
  t.window.borderless = false
  t.version = "11.1"
  t.release = "2.2"
  t.window.fullscreen = true
end
