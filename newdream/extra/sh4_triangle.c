// Returns 1 if point 'pt' is inside triangle with vertices 'v0', 'v1', and 'v2', and 0 if not
// Determines triangle center using barycentric coordinate transformation
// Adapted from: https://stackoverflow.com/questions/2049582/how-to-determine-if-a-point-is-in-a-2d-triangle
// Specifically the answer by user 'adreasdr' in addition to the comment by user 'urraka' on the answer from user 'Andreas Brinck'
#include "DreamHAL/inc/sh4_math.h"

/*
static inline __attribute__((always_inline)) int MATH_Point_Inside_Triangle(float v0.x, float v0.y, float v1.x, float v1.y, float v2.x, float v2.y, float pt.x, float pt.y)
{
  float sdot = MATH_fipr(v0.y, -v0.x, v2.y - v0.y, v0.x - v2.x, v2.x, v2.y, pt.x, pt.y);
  float tdot = MATH_fipr(v0.x, -v0.y, v0.y - v1.y, v1.x - v0.x, v1.y, v1.x, pt.x, pt.y);

  float areadot = MATH_fipr(-v1.y, v0.y, v0.x, v1.x, v2.x, -v1.x + v2.x, v1.y - v2.y, v2.y);

  // 'areadot' could be negative depending on the winding of the triangle
  if(areadot < 0.0f)
  {
    sdot *= -1.0f;
    tdot *= -1.0f;
    areadot *= -1.0f;
  }

  if( (sdot > 0.0f) && (tdot > 0.0f) && (areadot > (sdot + tdot)) )
  {
    return 1;
  }

  return 0;
}
*/
