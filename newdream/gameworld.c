#include <kos.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>

#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
#include "love2dream.h"
#include "gameworld.h"

int LUA_initGameworld(){
  lua_getglobal(luaData, "gameworld");
  lua_getfield(luaData, -1, "init");
  lua_pcall(luaData, 0, 1, 0);
  int status = lua_tonumber(luaData, -1);
  printf("L2D> Init gameworld : %d\n", status);

  if(status)
    GW_status = GW_EMPTY;
  else
    GW_status = GW_ERROR;

  lua_settop(luaData, 0);
  return 1;
}

int LUA_loadGameworld(char* file){
  lua_getglobal(luaData, "loadGameworld");
  lua_pushstring(luaData, file);
  int status = lua_pcall(luaData, 1, 1, 0);

  if(status == 0)
    printf("L2D> Loading %s file as gameworld\n", file);
  else
    printf("L2D> ERROR Loading %s file as gameworld <---------------\n", file);

  lua_settop(luaData, 0);
  return(status);
}

int LUA_createGameworld(char* startmap){
  lua_getglobal(luaData, "gameworld");
  lua_getfield(luaData, -1, "create");
  lua_pushstring(luaData, startmap);
  lua_pcall(luaData, 1, 1, 0);
  int status = lua_tonumber(luaData, -1);

  if(status)
    GW_status = GW_READY;
  else
    GW_status = GW_ERROR;

  printf("L2D> Create gameworld : %d\n", status);
  lua_settop(luaData, 0);
  return 1;
}

int LUA_freeGameworld(){
  lua_getglobal(luaData, "gameworld");
  lua_getfield(luaData, -1, "free");
  int result = lua_pcall(luaData, 0, 1, 0);
  if (result != 0)
    printf("Problem with LUA_freeGameworld\n");

  lua_settop(luaData, 0);
  return 1;
}

int LUA_updateGameworld(uint64_t deltaTime) {
  lua_getglobal(luaData, "gameworld");
  lua_getfield(luaData, -1, "update");
  float dt = deltaTime/1000.0f;
  lua_pushnumber(luaData, dt);
  int result = lua_pcall(luaData, 1, 1, 0);
  if (result != 0)
    printf("Problem with LUA_updateGameworld\n");
  lua_settop(luaData, 0);
  return 1;
}

int LUA_renderGameworld(uint64_t deltaTime) {
  lua_getglobal(luaData, "gameworld");
  lua_getfield(luaData, -1, "render");
  lua_pushnumber(luaData, deltaTime);
  int result = lua_pcall(luaData, 1, 1, 0);
  if (result != 0)
    printf("Problem with LUA_renderGameworld\n");
  lua_settop(luaData, 0);
  return 1;
}
