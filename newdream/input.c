#include <kos.h>
#include "love2dream.h"
#include "input.h"

char* playerName = "";
int   delayToExit = 30;
input *player[4];

int     initInput() {
  for(int i = 0; i < 4; i++) {
    player[i] = NULL;
  }


  lua_pushcfunction(luaData, LUA_getController);
  lua_setglobal(luaData, "C_getController");
  return(1);
}

input   *newController(int controllerNum) {
  if(controllerNum > 4 || controllerNum < 0) {
    printf("L2D-Input>  Player/Controller %d out of range!! \n", controllerNum);
    return(NULL);
  }
  input *temp = malloc(sizeof(input));

  temp->contNum    = controllerNum;
  temp->cont       = maple_enum_type(controllerNum, MAPLE_FUNC_CONTROLLER);
  temp->state      = (cont_state_t *)maple_dev_status(temp->cont);
  temp->pstate     = *temp->state;
  temp->pbuttons   = temp->buttons = 0;
  temp->update     = update;

  printf("L2D-Input>  Player/Controller %d added. \n", controllerNum);
  player[controllerNum] = temp;
  return temp;
}

void    update(input *self) {
  self->cont = maple_enum_type(self->contNum, MAPLE_FUNC_CONTROLLER);
  self->state = (cont_state_t *)maple_dev_status(self->cont);

  if((self->state->buttons & CONT_A) && (self->state->buttons & CONT_X)) {
    if(GW_status == GW_READY)
      GW_status = GW_RELOAD;
  }

  if(self->state->buttons & CONT_START) {
    delayToExit--;
    if (delayToExit == 1) {
      GW_status = GW_FREE;
    }
    if (delayToExit == 0)
      gameActive = 0;
  } else {
      delayToExit = 30;
  }

  self->buttons = self->state->buttons;
}

// update all the controllers?
void    updateControllers() {
  for(int i = 0; i < 4; i++) {
    if(player[i] != NULL) {
      update(player[i]);
      //printf("Updating player %d\n", i);
    }
  }
}

int     LUA_getController(lua_State *L) {
  input *p;
  int   controllerNum = lua_tonumber(L, 1) - 1; // -1 is lua offset

  if(controllerNum < 0 || controllerNum > 4) {
    printf("INPUT.C> Controller out of range (%d)\n", controllerNum);
    return(1);
  } else {
    p = player[controllerNum];
  }

  lua_pushnumber(L, p->state->buttons);
  lua_pushnumber(L, p->state->joyx);
  lua_pushnumber(L, p->state->joyy);
  lua_pushnumber(L, p->state->ltrig);
  lua_pushnumber(L, p->state->rtrig);
  return(5);
}

void    LUA_updateInput(input *self) {
  if(GW_status == GW_READY) {
    lua_getglobal(luaData, "_updateDreamcastController");
    lua_pushnumber(luaData, self->state->buttons);
    lua_pushnumber(luaData, self->state->joyx);
    lua_pushnumber(luaData, self->state->joyy);
    lua_pushnumber(luaData, self->state->ltrig);
    lua_pushnumber(luaData, self->state->rtrig);
    lua_pcall(luaData, 5, 1, 0);
  }
  //const char* r = lua_tostring(luaData, 1);
  //printf(r);

  lua_settop(luaData, 0);
  return(1);
}
