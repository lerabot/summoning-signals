#include <kos.h>
#include <GL/gl.h>
#include "love2dream.h"
#include "utils.h"

char*             gameworld   = "gw_summoning.lua";
char*             startmap    = "menu";
uint64_t          end_time, start_time, delta_time, game_time = 0;
int               debugActive = 1;
int               gameActive  = 1;
int               GW_status   = GW_EMPTY;
lua_State         *luaData;

input             *p1; //player 1

int main() {
  initLua(&luaData);
  initInput();
  initMath();
  initGL();
  initSound(MP3);
  initVMU();

  p1 = newController(0);
  initSaveload();
  initLantern(&luaData);

  // Load the gameword file
  LUA_loadGameworld(gameworld);

  // Init the gameworld (audio, video, input)
  LUA_initGameworld();
  while(gameActive)
  {
    start_time = getTime_MS();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glTranslatef(0, 480, 0);

    updateControllers();

    switch (GW_status) {
      case GW_READY:
        LUA_updateGameworld(delta_time);
        //glEnable(GL_LIGHTING);
        LUA_renderGameworld(delta_time);
        //glDisable(GL_LIGHTING);
        break;

      case GW_EMPTY: //If there's no world, create it
        LUA_createGameworld(startmap);
        //thd_sleep(200);
        break;

      case GW_FREE: //If the world need to be unloaded
        LUA_freeGameworld();
        //thd_sleep(200);
        printf("L2D> Gameworld free'd\n");
        GW_status = GW_ERROR;
        break;

      case GW_RELOAD: //If the world need to be unloaded
        printf("L2D> Attempt to restart\n");
        LUA_freeGameworld();
        thd_sleep(200);
        printf("L2D> Gameworld free'd\n");
        LUA_loadGameworld(gameworld);
        thd_sleep(200);
        LUA_createGameworld(startmap);
        GW_status = GW_READY;
        printf("L2D> Gameworld reloaded\n");
        break;
    }

    endFrame();
    glKosSwapBuffers();
    end_time = getTime_MS();
    delta_time = end_time - start_time;
    game_time += delta_time;
  }

  printf("Exiting game.\n");
  return(0);
}
