#include <kos.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <zlib/zlib.h>
#include <GL/gl.h>
#include <GL/glkos.h>
#include <GL/glext.h>
#include <GL/glu.h>
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"

#include "love2dream.h"

time_t  master_time;
char  *new_path[256];

char*     findFile(char *filename) {
  file_t  file;
  char  *dest[5];

  dest[0] = "";
  dest[1] = "cd";
  dest[2] = "pc";
  dest[3] = "/rd";
  dest[4] = "/sd";

  for(int i = 0; i < 5; i ++){
    sprintf(new_path, "%s/%s", dest[i], filename);
    if ((file = fs_open(new_path, O_RDONLY)) != -1){
      if (debugActive) printf("Found file %s at %s > %s\n", filename, dest[i], new_path);
      fs_close(file);
      return(new_path);
    } else {
      //printf("No file %s at %s > %s%s\n", filename, dest[i], dest[i], filename);
    }
  }
  if (debugActive) printf("No file %s \n", filename);
  return(NULL);
}

void      debugMess(char *message) {
  if(debugActive){
    printf(message);
  }
}

int       mount_romdisk(char *filename, char *mountpoint){
  void  *buffer;
  char  path[100];
  int   length = 0;
  char  *dest[3];
  file_t f;

  dest[0] = "/pc";
  dest[1] = "/cd";
  dest[2] = "/sd";

  for(int i = 0; i < 3; i ++){
    sprintf(path, "%s/%s", dest[i], filename);
    f = fs_open(path, O_RDONLY);
    if(f != -1) {
      length = fs_total(f);
      printf("Found romdisk at %s -> size : %u\n", dest[i], length);
      break;
    } else {
      printf("Looking for romdisk at %s/%s\n", dest[i], filename);
    }
  }
  fs_close(f);

  ssize_t size = fs_load(path, &buffer);
  // Successfully read romdisk image
  if(size != -1)
  {
    fs_romdisk_mount(mountpoint, buffer, 1);
    return(1);
  }
  return(0);
}

int       unmount_romdisk() {
  if (fs_romdisk_unmount("/rd") == 0) {
    return(1);
  }
  else {
    printf("Failed to unmount romdisk\n");
    return(0);
  }
  return 0;
}

int       LUA_mountRomdisk(lua_State *L) {
    const char* filename = lua_tostring(L, 1);
    const char* mountpoint = lua_tostring(L, 2);

    char* f = findFile(filename);
    if(f == NULL) return(NULL);

    int result = mount_romdisk(filename, mountpoint);

    lua_pushnumber(L, result);
    return(1);
}

int       LUA_unmountRomdisk(lua_State *L) {
    unmount_romdisk();

    return(1);
}

int       loadFile(char *filename) {
  void *buffer;
  int size = fs_load(filename, &buffer);

  // Successfully read romdisk image
  if(size != -1)
  {
      //fs_romdisk_mount(mountpoint, buffer, 1);
      return size;
  }
  else
      return 0;
}

uint64_t  getTime_MS() {
    uint32 s_s, s_ms;
    timer_ms_gettime(&s_s, &s_ms);
    return s_s*1000 + s_ms;
}

uint64_t  getTime_US() {
    uint64 us;
    us = timer_us_gettime64();
    return us;
}

int       LUA_clock(lua_State *L) {
  lua_pushnumber(L, getTime_MS());
  return 1;
}

int       LUA_clockUS(lua_State *L) {
  lua_pushnumber(L, getTime_US());
  return 1;
}

void      quitGame(){
    exit(1);
}

// LUA /////////////////////////////////////////////
void      initLua(lua_State **L_state) {
  *L_state = luaL_newstate();

  luaL_openlibs(*L_state);
  luaL_requiref( *L_state, "_G", luaopen_base, 1 );
  luaL_requiref( *L_state, "_G", luaopen_math, 1 );
  luaL_requiref( *L_state, "_G", luaopen_string, 1 );
  luaL_requiref( *L_state, "_G", luaopen_table, 1 );
  luaL_requiref( *L_state, "_G", luaopen_os, 1 );
  luaL_requiref( *L_state, "_G", luaopen_io, 1 );

  lua_pushcfunction(*L_state, LUA_clock);
  lua_setglobal(*L_state, "C_clock");

  lua_pushcfunction(*L_state, LUA_clockUS);
  lua_setglobal(*L_state, "C_clockUS");


}

int       initLantern(lua_State **L_state) {
  lua_pushcfunction(*L_state, LUA_mountRomdisk);
  lua_setglobal(luaData, "C_mountRomdisk");

  lua_pushcfunction(*L_state, LUA_unmountRomdisk);
  lua_setglobal(luaData, "C_unmountRomdisk");

  lua_pushstring(*L_state, "DC");
  lua_setglobal(*L_state,  "platform");
  char *path = findFile("platform.lua");
  int r = loadLuaFile(*L_state,    path);
  /*
  if(r == 1)
    printf("L2D-Lua> OK platform.lua\n");
  else
    printf("L2D-Lua> Failed to run platform.lua\n");
  */

  lua_settop(*L_state, 0);

  lua_getglobal(*L_state, "initPlatform");
  lua_pcall(*L_state, 0, 1, 0);
  int status = lua_tonumber(*L_state, 1);

  if(status == 1){
    printf("L2D-Lua> Init done\n");
  } else {
    printf("L2D-Lua> Init failed\n");
    exit(0);
  }
}

/*
int       reloadGameworld(lua_State **L_state, const char* gameWorld) {
  lua_getglobal(*L_state, "platformInit");
  lua_pushstring(*L_state, gameWorld);
  lua_pcall(*L_state, 1, 1, 0);
  int status = lua_tonumber(*L_state, 1);
  lua_settop(*L_state, 0);

  LUA_createGameworld();
  return(1);
}

int       reloadEngine(lua_State **L_state) {
  int result = 0;
  int r = loadLuaFile(*L_state,    findFile("/platform.lua"));
  lua_settop(*L_state, 0);

  lua_getglobal(*L_state, "platformInit");
  lua_pcall(*L_state, 0, 0, 0);
  lua_settop(*L_state, 0);

  LUA_createGameworld();
  r = GW_READY;
  return(r);
}
*/
int       loadLuaFile(lua_State *L_state, char *filename) {
  if(luaL_loadfile(L_state, filename) || lua_pcall(L_state, 0, 0, 0)) {
    printf(lua_tostring(L_state, -1));
    return(0);
  }
  return (1);
}

int       doLuaFile(lua_State *L_state, char *filename) {
  if(luaL_loadfile(L_state, filename) || lua_pcall(L_state, 0, 0, 0)) {
    return(0);
  }
  return (1);
}
