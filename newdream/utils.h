#ifndef __UTILS_H__
#define __UTILS_H__

#include "lua.h"

char*   findFile(char* filename);
void    debugMess(char *message);
int     unmount_romdisk();
int     mount_romdisk(char *filename, char *mountpoint);
int     loadFile(char *filename);

// LUA ///////////////////////////////
void    initLua(lua_State **L_state);
int     initLantern(lua_State **L_state);
int     reloadGameworld(lua_State **L_state, const char* gameWorld);
int     reloadEngine(lua_State **L_state);
int     loadLuaFile(lua_State *L_state, char *filename);
void    setLuaState(lua_State *L_state);


uint64_t  getTime_MS();
float     getTime_SEC();
void    quitGame();
//MATH//////////////
double distance(float x1,float y1,float x2,float y2);


#endif
