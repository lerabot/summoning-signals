#include <kos.h>
#include "love2dream.h"
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
#include "vmu.h"

#include "crayonVMU/savefile.h"
#include "crayonVMU/setup.h"

uint8_t                   *vmu_icons[256] = { NULL };
crayon_savefile_details_t vmu_details;
uint8_t                   vmu_data; //we don't store anything here for real.'

int initVMU() {
  unsigned int r = 1;
  crayon_savefile_init_savefile_details(&vmu_details, (uint8_t)&vmu_data, 1, 0, 0, "", "", "", "");
  printf("VMU> Init done: %u\n", r);

  char* logo  = findFile("asset/VMU/logo.bin");
  int   index = getEmptyIcon();
  setup_vmu_icon_load(  &vmu_icons[index], logo);
  crayon_vmu_display_icon(vmu_details.valid_vmu_screens,  vmu_icons[index]);

  lua_pushcfunction(luaData, LUA_loadVMUIcon);
  lua_setglobal(luaData, "C_loadVMUIcon");

  lua_pushcfunction(luaData, LUA_freeVMUIcon);
  lua_setglobal(luaData, "C_freeVMUIcon");

  lua_pushcfunction(luaData, LUA_drawVMUIcon);
  lua_setglobal(luaData, "C_drawVMUIcon");

  lua_pushcfunction(luaData, LUA_clearVMUIcon);
  lua_setglobal(luaData, "C_clearVMUIcon");


  return(r);
}

int LUA_loadVMUIcon(lua_State *L) {
  const char* logo  = lua_tostring(L, 1);
  int         index = getEmptyIcon();

  char* path = findFile(logo);
  int r = setup_vmu_icon_load( &vmu_icons[index], path);
  printf("VMU> Loading icon %s at ID:%u\n", path, index);

  lua_pushnumber(L, index);
  return 1;
}

int LUA_freeVMUIcon(lua_State *L) {
  int index = (int)lua_tonumber(L, 1);

  if(vmu_icons[index] != NULL) {

  }
  return 1;
}

int LUA_drawVMUIcon(lua_State *L) {
  int index = (int)lua_tonumber(L, 1);

  if (vmu_icons[index] != NULL) {
    //printf("VMU> drawing ID:%u\n", index);
    crayon_vmu_display_icon(vmu_details.valid_vmu_screens,  vmu_icons[index]);
  }
  return 1;
}

int LUA_clearVMUIcon(lua_State *L) {
  return 1;
}

int getEmptyIcon() {
  for(int i=0; i < 256; i++) {
    if(vmu_icons[i] == NULL){
      return(i);
    }
  }
  return(-1);
}
