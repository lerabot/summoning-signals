#ifndef __VMU_H__
#define __VMU_H__

#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"

int initVMU();

int LUA_loadVMUIcon(lua_State *L);
int LUA_freeVMUIcon(lua_State *L);
int LUA_drawVMUIcon(lua_State *L);
int LUA_clearVMUIcon(lua_State *L);

#endif
