#!/bin/sh

#https://linuxize.com/post/how-to-rename-files-in-linux/

#for f in sounds/*.{wav,WAV}; do ffmpeg -i "$f" -c:a libvorbis -q:a 4 "sounds/${f%.*}.ogg"; done
#'ffmpeg -i "$0" -c:a libvorbis -q:a 4 "${f/%wav/ogg}"' '{}'

#############################################################
#find asset/sounds -type f -iname '._*.wav'  -exec bash -c 'rm "{}"' \;

find asset/sounds -type f -name '*.ogg'     -exec sh -c '

  f="{}"; ffmpeg -y -i "{}" -ar 22050 "{}.wav"

  ' \;

find asset/sounds -name '*.ogg.wav' -print0 | xargs -0 -n1 bash -c 'mv "$0" "${0/ogg.wav/wav}"'
#find asset/sounds -type f -iname '*.wav' -exec bash -c 'rm "{}"' \;
